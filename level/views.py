from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect
from .forms import *

# Create your views here.
def daftar_level(request):
	response = {}

	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM LEVEL_KEANGGOTAAN")
		response["list_level"] = cursor.fetchall()

	return render(request, 'daftar_level.html', response)

def tambah_level(request):
	if (request.method == "POST"):
		form = LevelForm(request.POST)
		if(form.is_valid()):
			with connection.cursor() as cursor:
				nama_level = request.POST['nama_level']
				min_poin = request.POST['min_poin']
				deskripsi = request.POST['deskripsi']
				cursor.execute("INSERT INTO LEVEL_KEANGGOTAAN VALUES(%s, %s, %s)", [nama_level, min_poin, deskripsi])

			return redirect("/level/")

	form = LevelForm()
	response = {}
	response["level_form"] = form
	return render(request, 'tambah_level.html', response)

def update_level(request, nama):
	data = {}
	response = {}

	if (request.method == "POST"):
		form = LevelForm(request.POST)
		if (form.is_valid()):
			with connection.cursor() as cursor:
				nama_level = request.POST['nama_level']
				min_poin = request.POST['min_poin']
				deskripsi = request.POST['deskripsi']
				cursor.execute("UPDATE LEVEL_KEANGGOTAAN SET nama_level = %s, minimum_poin = %s, deskripsi = %s WHERE nama_level = %s", [nama_level, min_poin, deskripsi, nama])

			return redirect("/level/")

	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM LEVEL_KEANGGOTAAN K WHERE K.nama_level = %s", [nama])
		data_level = cursor.fetchone()

	data["nama_level"] = data_level[0]
	data["min_poin"] = data_level[1]
	data["deskripsi"] = data_level[2]

	form = LevelForm(data)
	print(form)
	response["level_form"] = form
	response["nama"] = nama

	return render(request, 'update_level.html', response)

def delete_level(request, nama):
	with connection.cursor() as cursor:
		cursor.execute("DELETE FROM LEVEL_KEANGGOTAAN K WHERE K.nama_level = %s", [nama])
	return redirect("/level/")
