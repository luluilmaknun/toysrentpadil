from django.urls import path
from level import views

app_name = 'level'
urlpatterns = [
	path('', views.daftar_level, name='list_level'),
	path('add', views.tambah_level, name='add_level'),
	path('<nama>/update', views.update_level, name='update_level'),
	path('<nama>/delete', views.delete_level, name='delete_level')
]
