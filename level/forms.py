from django import forms

class LevelForm(forms.Form):
    nama_level = forms.CharField(label='Nama Level', widget=forms.TextInput(attrs={'class': 'form-control col-md-6',
        'id': 'nama-level', 'placeholder': "Nama Level"}))

    min_poin = forms.IntegerField(label='Minimum Poin', widget=forms.NumberInput(attrs={'class': 'form-control col-md-6',
        'id': 'min-point', 'placeholder': "Minimum Poin"}))

    deskripsi = forms.CharField(label='Deskripsi', widget=forms.TextInput(attrs={'class': 'form-control col-md-6',
                                                                'id': 'deskripsi', 'placeholder': "Deskripsi"}))