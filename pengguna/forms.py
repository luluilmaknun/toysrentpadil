from django import forms

class login_form(forms.Form):
    no_ktp = forms.CharField(label = '', max_length=20, widget = forms.TextInput(attrs={'class': 'form-control Font-Size-20',
        'id': 'ktp', 'placeholder': "Nomor KTP"}))

    email = forms.CharField(label = '', max_length=225, widget = forms.EmailInput(attrs={'class': 'form-control Font-Size-20',
        'id': 'email', 'placeholder': "Email"}))

class register_form(forms.Form):
    no_ktp = forms.CharField(label = '', max_length=20, widget = forms.TextInput(attrs={'class': 'form-control Font-Size-20',
        'id': 'ktp', 'placeholder': "Nomor KTP"}))

    nama_lengkap = forms.CharField(label = '', max_length=225, widget = forms.TextInput(attrs={'class': 'form-control Font-Size-20',
        'id': 'nama_lengkap', 'placeholder': "Nama Lengkap"}))

    tanggal_lahir = forms.DateField(label = '', widget = forms.DateInput(attrs={'type': 'text',
        'onfocus': "(this.type = 'date')", 'class': 'form-control Font-Size-20', 'placeholder': "Tanggal Lahir"}))

    email = forms.CharField(label = '', max_length=225, widget = forms.EmailInput(attrs={'class': 'form-control Font-Size-20',
        'id': 'email', 'placeholder': "Email"}))

    no_telp = forms.CharField(label = '', max_length=20, widget = forms.TextInput(attrs={'class': 'form-control Font-Size-20',
        'id': 'no_telp', 'placeholder': "Namor telepon"}))