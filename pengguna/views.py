from django.shortcuts import render
from .forms import *
from django.db import connection
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import redirect
from django.forms import formset_factory

def login_page(request):
	form = login_form()
	return render(request, 'login.html', {'form': form})

def user_login(request):
	form = login_form(request.POST or None)
	if (request.method == "POST" and form.is_valid()):
			no_ktp = request.POST['no_ktp']
			email = request.POST['email']
			if (login(request, no_ktp, email)):
				if request.session['role'] == 'Anggota':
					return redirect("/barang")
				elif request.session['role'] == 'Admin':
					return redirect("/pesanan")
			else:
				form.add_error("no_ktp", "Akun ini tidak ada!")
				return render(request, 'login.html', {'form':form})

def login(request, no_ktp, email):
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM PENGGUNA WHERE no_ktp = %s AND email = %s", [no_ktp, email])
		row = cursor.fetchone()

		if (row != None):
			request.session['no_ktp'] = row[0]
			request.session['nama'] = row[1]
			request.session['email'] = row[2]
			request.session['tanggal_lahir'] = json.dumps(row[3], cls=DjangoJSONEncoder)[1:-1]
			request.session['no_telepon'] = row[4]
			request.session['role'] = check_role(no_ktp)
			if (request.session['role'] == "Anggota"):
				alamats = get_alamat_user(no_ktp)
				request.session['alamats'] = []
				for alamat in alamats:
					request.session['alamats'].append(alamat[1] + " - " + alamat[2] + " Street No. " \
													  + str(alamat[3]) + ", " + alamat[4] + " City, " \
													  + alamat[5])
				cursor.execute("SELECT * FROM ANGGOTA WHERE no_ktp = %s", [no_ktp])
				row = cursor.fetchone()
				request.session['poin'] = row[1]
				request.session['level'] = row[2]
			return True
		elif (row == None):
			return False

def check_availability(no_ktp, email):
	available = [True, True]

	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM PENGGUNA WHERE no_ktp = %s", [no_ktp])
		ktp_row = cursor.fetchone()
		if ktp_row != None:
			available[0] = False

		cursor.execute("SELECT * FROM PENGGUNA WHERE email = %s", [email])
		email_row = cursor.fetchone()
		if email_row != None:
			available[1] = False
	print(available)
	return available

def check_role(no_ktp):
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM ADMIN WHERE no_ktp = %s", [no_ktp])
		if (cursor.fetchone() != None):
			return "Admin"
		return "Anggota"

def get_alamat_user(no_ktp):
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM ALAMAT WHERE no_ktp_anggota = %s", [no_ktp])
		return cursor.fetchall()

def logout(request):
	request.session.flush()
	return redirect("/")

def register_page(request, role):
	response = {}
	response['role'] = role
	response['register_form'] = register_form()
	return render(request, 'register.html', response)

def user_register(request, role):
	rgst_form = register_form(request.POST or None)
	if (request.method == "POST" and rgst_form.is_valid()):
		with connection.cursor() as cursor:
			no_ktp = request.POST['no_ktp']
			nama_lengkap = request.POST['nama_lengkap']
			tanggal_lahir = request.POST['tanggal_lahir']
			email = request.POST['email']
			no_telp = request.POST['no_telp']

			if (check_availability(no_ktp, email)[0] == False) or (check_availability(no_ktp, email)[1] == False):
				if (check_availability(no_ktp, email)[0] == False):
					rgst_form.add_error("nama_lengkap", "* Kamu tidak dapat mendaftar dengan nomor KTP ini!")
				if (check_availability(no_ktp, email)[1] == False):
					rgst_form.add_error("no_telp", "* Kamu tidak dapat mendaftar dengan email ini!")
				response = {}
				response['role'] = role
				response['register_form'] = rgst_form
				return render(request, 'register.html', response)


			cursor.execute("INSERT INTO PENGGUNA VALUES(%s, %s, %s, %s, %s)", [no_ktp, nama_lengkap, email, tanggal_lahir, no_telp])
			if role == "admin":
				cursor.execute("INSERT INTO ADMIN VALUES(%s)", [no_ktp])
			elif role == "anggota":
				cursor.execute("INSERT INTO ANGGOTA VALUES(%s, %s, %s)", [no_ktp, '0', 'Bronze'])
				alamat_key = list(request.POST)[6::]
				register_alamat(request, alamat_key, no_ktp)
			login(request, no_ktp, email)
			if request.session['role'] == 'Anggota':
				return redirect("/barang")
			elif request.session['role'] == 'Admin':
				return redirect("/pesanan")

def register_alamat(request, alamat_key, no_ktp):
	with connection.cursor() as cursor:
		for i in range(len(alamat_key)):
			if i % 5 == 0 and i != 0:
				nama_alamat = request.POST[alamat_key[i]]
				jalan = request.POST[alamat_key[i+1]]
				nomor = request.POST[alamat_key[i+2]]
				kota = request.POST[alamat_key[i+3]]
				kodepos = request.POST[alamat_key[i+4]]
				cursor.execute("INSERT INTO ALAMAT VALUES(%s, %s, %s, %s, %s, %s)", [no_ktp, nama_alamat, jalan, nomor, kota, kodepos])


def profil_page(request):
	return render(request, 'profil.html')