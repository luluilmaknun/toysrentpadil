from django.conf.urls import url
from django.urls import path
from .views import *
#url for app

app_name = 'pengguna'
urlpatterns = [
    url(r'^login', login_page, name='login_page'),
    path('register/<str:role>/', register_page, name='register_page'),
    url(r'^user_login', user_login, name='user_login'),
    path('user_register/<str:role>', user_register, name='user_register'),
    url(r'^profil', profil_page, name='profil_page'),
    url(r'^logout', logout, name='logout'),
]