--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 11.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: toys_rent; Type: SCHEMA; Schema: -; Owner: db2018025
--

CREATE SCHEMA toys_rent;


--
-- Name: update_kondisi_barang(); Type: FUNCTION; Schema: public; Owner: db2018025
--

CREATE FUNCTION public.update_kondisi_barang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF(TG_OP = 'INSERT') THEN
UPDATE BARANG B SET kondisi = 'sedang disewa anggota'
  FROM BARANG_PESANAN BP
JOIN PEMESANAN P
  ON P.id_pemesanan = BP.id_pemesanan
  WHERE B.id_barang = BP.id_barang AND P.id_pemesanan = NEW.id_pemesanan;
RETURN NEW;
ELSIF(TG_OP = 'DELETE') THEN
UPDATE BARANG B SET kondisi = 'batal disewa'
  FROM BARANG_PESANAN BP
JOIN PEMESANAN P
  ON P.id_pemesanan = BP.id_pemesanan
  WHERE B.id_barang = BP.id_barang AND P.id_pemesanan = OLD.id_pemesanan;
RETURN OLD;
ELSIF(TG_OP = 'UPDATE') THEN
UPDATE BARANG B SET kondisi = 'sedang disewa anggota'
  FROM BARANG_PESANAN BP
JOIN PEMESANAN P
  ON P.id_pemesanan = BP.id_pemesanan
  WHERE B.id_barang = BP.id_barang AND P.id_pemesanan = NEW.id_pemesanan;

UPDATE BARANG B SET kondisi = 'batal disewa'
  FROM BARANG_PESANAN BP
JOIN PEMESANAN P
  ON P.id_pemesanan = BP.id_pemesanan
  WHERE B.id_barang = BP.id_barang AND P.id_pemesanan = OLD.id_pemesanan;
RETURN NEW;
END IF;
END;
$$;

--
-- Name: update_harga_sewa(); Type: FUNCTION; Schema: toys_rent; Owner: db2018025
--

CREATE FUNCTION toys_rent.update_harga_sewa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
 sewa_baru INTEGER;
 sewa_lama INTEGER;
BEGIN
 SELECT IBL.harga_sewa INTO sewa_baru
 FROM PEMESANAN P, BARANG B, INFO_BARANG_LEVEL IBL, 
 LEVEL_KEANGGOTAAN LK, ANGGOTA A
 WHERE NEW.id_pemesanan = P.id_pemesanan
 AND P.no_ktp_pemesan = A.no_ktp
 AND A.level = LK.nama_level
 AND LK.nama_level = IBL.nama_level
 AND NEW.id_barang = B.id_barang
 AND B.id_barang = IBL.id_barang;

 IF (TG_OP = 'INSERT') THEN
  UPDATE PEMESANAN
  SET harga_sewa = harga_sewa + sewa_baru
  WHERE id_pemesanan = NEW.id_pemesanan;
  RETURN NEW;

 ELSIF (TG_OP = 'DELETE') THEN
  SELECT IBL.harga_sewa INTO sewa_lama
  FROM PEMESANAN P, BARANG B, INFO_BARANG_LEVEL IBL,
  LEVEL_KEANGGOTAAN LK, ANGGOTA A
  WHERE OLD.id_pemesanan = P.id_pemesanan
  AND P.no_ktp_pemesan = A.no_ktp
  AND A.level = LK.nama_level
  AND LK.nama_level = IBL.nama_level
  AND OLD.id_barang = B.id_barang
  AND B.id_barang = IBL.id_barang;
  UPDATE PEMESANAN
  SET harga_sewa = harga_sewa - sewa_lama
  WHERE id_pemesanan = OLD.id_pemesanan;
  RETURN OLD;

 ELSIF (TG_OP = 'UPDATE') THEN
  SELECT IBL.harga_sewa INTO sewa_lama
  FROM PEMESANAN P, BARANG B, INFO_BARANG_LEVEL IBL,
  LEVEL_KEANGGOTAAN LK, ANGGOTA A
  WHERE OLD.id_pemesanan = P.id_pemesanan
  AND P.no_ktp_pemesan = A.no_ktp
  AND A.level = LK.nama_level
  AND LK.nama_level = IBL.nama_level
  AND OLD.id_barang = B.id_barang
  AND B.id_barang = IBL.id_barang;
  UPDATE PEMESANAN
  SET harga_sewa = harga_sewa - sewa_lama
  WHERE id_pemesanan = OLD.id_pemesanan;

  UPDATE PEMESANAN
  SET harga_sewa = harga_sewa + sewa_baru
  WHERE id_pemesanan = NEW.id_pemesanan;
  RETURN NEW;
 END IF;
END;
$$;

--
-- Name: update_kondisi_barang(); Type: FUNCTION; Schema: toys_rent; Owner: db2018025
--

CREATE FUNCTION toys_rent.update_kondisi_barang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF(TG_OP = 'INSERT') THEN
UPDATE BARANG B SET kondisi = 'sedang disewa anggota'
  FROM BARANG_PESANAN BP
JOIN PEMESANAN P
  ON P.id_pemesanan = BP.id_pemesanan
  WHERE B.id_barang = BP.id_barang AND P.id_pemesanan = NEW.id_pemesanan AND NEW.id_barang = B.id_barang;
RETURN NEW;
ELSIF(TG_OP = 'DELETE') THEN
UPDATE BARANG B SET kondisi = 'batal disewa'
  FROM BARANG_PESANAN BP
JOIN PEMESANAN P
  ON P.id_pemesanan = BP.id_pemesanan
  WHERE B.id_barang = BP.id_barang AND P.id_pemesanan = OLD.id_pemesanan AND OLD.id_barang = B.id_barang;
RETURN OLD;
ELSIF(TG_OP = 'UPDATE') THEN
UPDATE BARANG B SET kondisi = 'sedang disewa anggota'
  FROM BARANG_PESANAN BP
JOIN PEMESANAN P
  ON P.id_pemesanan = BP.id_pemesanan
  WHERE B.id_barang = BP.id_barang AND P.id_pemesanan = NEW.id_pemesanan AND NEW.id_barang = B.id_barang;

UPDATE BARANG B SET kondisi = 'batal disewa'
  FROM BARANG_PESANAN BP
JOIN PEMESANAN P
  ON P.id_pemesanan = BP.id_pemesanan
  WHERE B.id_barang = BP.id_barang AND P.id_pemesanan = OLD.id_pemesanan AND OLD.id_barang = B.id_barang;
RETURN NEW;
END IF;
END;
$$;

--
-- Name: update_level_keanggotaan(); Type: FUNCTION; Schema: toys_rent; Owner: db2018025
--

CREATE FUNCTION toys_rent.update_level_keanggotaan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
IF (TG_OP = 'INSERT') THEN
PERFORM update_level_keanggotaan_semua_anggota();
RETURN NEW;

ELSIF (TG_OP = 'DELETE') THEN
PERFORM update_level_keanggotaan_semua_anggota();
RETURN OLD;

ELSIF (TG_OP = 'UPDATE') THEN
PERFORM update_level_keanggotaan_semua_anggota();
RETURN NEW;

END IF;
END
$$;

--
-- Name: update_level_keanggotaan(character); Type: FUNCTION; Schema: toys_rent; Owner: db2018025
--

CREATE FUNCTION toys_rent.update_level_keanggotaan(no_ktp_anggota character) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE temp_row RECORD;
nama_level CHAR;

BEGIN
FOR temp_row in 
SELECT LK.nama_level AS nama_level, 
LK.minimum_poin AS minimum_poin, A.poin AS poin 
FROM LEVEL_KEANGGOTAAN LK, ANGGOTA A
WHERE A.no_ktp = no_ktp_anggota
ORDER BY minimum_poin DESC
LOOP
IF (temp_row.poin >= temp_row.minimum_poin) THEN 
UPDATE ANGGOTA A
SET level = temp_row.nama_level
WHERE A.no_ktp = no_ktp_anggota;
RETURN temp_row.nama_level;
EXIT;
ELSE
UPDATE ANGGOTA A
SET level = NULL
WHERE A.no_ktp = no_ktp_anggota;
nama_level = Null;
END IF;
END LOOP;
RETURN nama_level;
END;
$$;


--
-- Name: update_level_keanggotaan_anggota(); Type: FUNCTION; Schema: toys_rent; Owner: db2018025
--

CREATE FUNCTION toys_rent.update_level_keanggotaan_anggota() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
IF (TG_OP = 'INSERT') THEN
PERFORM update_level_keanggotaan(NEW.no_ktp);
RETURN NEW;

ELSIF (TG_OP = 'UPDATE') THEN
PERFORM update_level_keanggotaan(NEW.no_ktp);
PERFORM update_level_keanggotaan(OLD.no_ktp);
RETURN NEW;

END IF;
END;
$$;

--
-- Name: update_level_keanggotaan_semua_anggota(); Type: FUNCTION; Schema: toys_rent; Owner: db2018025
--

CREATE FUNCTION toys_rent.update_level_keanggotaan_semua_anggota() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE temp_row RECORD;
BEGIN
FOR temp_row IN SELECT A.no_ktp AS no_ktp FROM ANGGOTA A
LOOP
PERFORM update_level_keanggotaan(temp_row.no_ktp);
END LOOP;
END;
$$;


--
-- Name: update_ongkos(); Type: FUNCTION; Schema: toys_rent; Owner: db2018025
--

CREATE FUNCTION toys_rent.update_ongkos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
 IF (TG_OP = 'INSERT') THEN
  UPDATE PEMESANAN
  SET ongkos = ongkos + NEW.ongkos
  WHERE id_pemesanan = NEW.id_pemesanan;
  RETURN NEW;

 ELSIF (TG_OP = 'DELETE') THEN
  UPDATE PEMESANAN
  SET ongkos = ongkos - OLD.ongkos
  WHERE id_pemesanan = OLD.id_pemesanan;
  RETURN OLD;

 ELSIF (TG_OP = 'UPDATE') THEN
  UPDATE PEMESANAN
  SET ongkos = ongkos - OLD.ongkos
  WHERE id_pemesanan = OLD.id_pemesanan;

  UPDATE PEMESANAN
  SET ongkos = ongkos + NEW.ongkos
  WHERE id_pemesanan = NEW.id_pemesanan;
  RETURN NEW;
 END IF;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.admin (
    no_ktp character varying(20) NOT NULL
);

--
-- Name: alamat; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.alamat (
    no_ktp_anggota character varying(20) NOT NULL,
    nama character varying(255) NOT NULL,
    jalan character varying(255) NOT NULL,
    nomor integer NOT NULL,
    kota character varying(255) NOT NULL,
    kodepos character varying(10) NOT NULL
);

--
-- Name: anggota; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.anggota (
    no_ktp character varying(20) NOT NULL,
    poin real NOT NULL,
    level character varying(20)
);

--
-- Name: barang; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.barang (
    id_barang character varying(10) NOT NULL,
    nama_item character varying(255) NOT NULL,
    warna character varying(50),
    url_foto text,
    kondisi text NOT NULL,
    lama_penggunaan integer,
    no_ktp_penyewa character varying(20) NOT NULL
);

--
-- Name: barang_dikembalikan; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.barang_dikembalikan (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10) NOT NULL
);

--
-- Name: barang_dikirim; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.barang_dikirim (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10) NOT NULL,
    tanggal_review date NOT NULL,
    review text NOT NULL
);

--
-- Name: barang_pesanan; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.barang_pesanan (
    id_pemesanan character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10) NOT NULL,
    tanggal_sewa date NOT NULL,
    lama_sewa integer NOT NULL,
    tanggal_kembali date,
    status character varying(50) NOT NULL
);

--
-- Name: chat; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.chat (
    id character varying(15) NOT NULL,
    pesan text NOT NULL,
    date_time timestamp without time zone NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    no_ktp_admin character varying(20) NOT NULL
);

--
-- Name: info_barang_level; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.info_barang_level (
    id_barang character varying(10) NOT NULL,
    nama_level character varying(20) NOT NULL,
    harga_sewa real NOT NULL,
    porsi_loyalti real NOT NULL
);

--
-- Name: item; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.item (
    nama character varying(255) NOT NULL,
    deskripsi text,
    usia_dari integer NOT NULL,
    usia_sampai integer NOT NULL,
    bahan text
);

--
-- Name: kategori; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.kategori (
    nama character varying(255) NOT NULL,
    level integer,
    sub_dari character varying(255)
);

--
-- Name: kategori_item; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.kategori_item (
    nama_item character varying(255) NOT NULL,
    nama_kategori character varying(255) NOT NULL
);

--
-- Name: level_keanggotaan; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.level_keanggotaan (
    nama_level character varying(20) NOT NULL,
    minimum_poin real NOT NULL,
    deskripsi text
);

--
-- Name: pemesanan; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.pemesanan (
    id_pemesanan character varying(10) NOT NULL,
    datetime_pesanan timestamp without time zone NOT NULL,
    kuantitas_barang integer NOT NULL,
    harga_sewa real,
    ongkos real,
    no_ktp_pemesan character varying(20) NOT NULL,
    status character varying(50) NOT NULL
);

--
-- Name: pengembalian; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.pengembalian (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10) NOT NULL,
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    nama_alamat_anggota character varying(255) NOT NULL
);

--
-- Name: pengguna; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.pengguna (
    no_ktp character varying(20) NOT NULL,
    nama_lengkap character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    tanggal_lahir date,
    no_telp character varying(20)
);

--
-- Name: pengiriman; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.pengiriman (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10) NOT NULL,
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    nama_alamat_anggota character varying(255) NOT NULL
);

--
-- Name: status; Type: TABLE; Schema: toys_rent; Owner: db2018025
--

CREATE TABLE toys_rent.status (
    nama text NOT NULL,
    deskripsi text
);

--
-- Data for Name: admin; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.admin (no_ktp) FROM stdin;
846603383
723132618
667334564
476641275
707580576
102287558
606335605
143882080
181044377
12657044
\.


--
-- Data for Name: alamat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.alamat (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) FROM stdin;
531146331	House	Center	1	Grand Island	808933
65787275	Office	Coolidge	3	Kamalia	C1A 0W1
260351280	Office	Hoffman	2364	Cantley	9716
272253762	House	Myrtle	4459	Ferrazzano	74641
600737262	Apartment	Hoffman	56040	Beverlo	7925
656374076	House	Judy	208	VitrysurSeine	62029
832805316	Apartment	Sunfield	13	Bergisch Gladbach	G9A 0B6
537106202	Estate	Rieder	670	Monte Vidon Corrado	5312
223650412	House	Northridge	36	Padre las Casas	83732
741010433	Office	Wayridge	5995	Ruddervoorde	D39 8KO
527034322	Dorm	Harper	9269	Detroit	E1M 1A0
251367872	Office	Hanson	11963	Rouen	15761
766878206	House	Hanover	61	Casole d'Elsa	3944
503723662	Apartment	Utah	7	Rendsburg	8286
314631664	Apartment	Trailsway	1416	New Glasgow	284949
385158543	Dorm	Briar Crest	348	Beaumont	GK2J 5JV
102250071	Office	Oakridge	808	Alvito	2903 SF
844441486	Office	Chinook	326	Filignano	244230
754822233	Dorm	Dottie	86	Vezirköprü	9435
47578854	House	Artisan	11	Cirencester	14210
721342626	Dorm	Bay	2524	Bhubaneswar	16481
683126206	Dorm	Pond	124	Bahawalnagar	87067434
43218620	House	Monica	37456	Penhold	28368
283155418	Office	Quincy	3817	Vergemoli	4936 TV
353707750	Office	Cherokee	8	Brunn am Gebirge	84808
635325146	Dorm	Prentice	1	WalsSiezenheim	56829
278252278	House	Miller	324	Fairbanks	99000
758864457	Apartment	Moose	70	Vlimmeren	1665
716231711	Dorm	Charing Cross	3	Colchester	4653
326388820	Dorm	Little Fleur	92334	Memphis	224075
203456425	Office	Anderson	767	Foz do Iguaçu	27475
273327253	Dorm	Shasta	28169	Monte San Giovanni in Sabina	63452
115273830	Dorm	2nd	77	Okene	95326
35242664	House	6th	6650	Northallerton	1435
631128376	House	Emmet	4546	Gdynia	51747660
607146165	Dorm	Talisman	4560	Antártica	9158
254150242	Office	Corry	16	Comeglians	63582
117303313	Dorm	Crest Line	9750	Abbotsford	26468
82101555	Apartment	Debra	183	Tailles	20169
456153051	Dorm	Springs	1551	Tollembeek	96090
642371487	House	Stephen	848	Jodoigne	795014
75314253	Dorm	Lakewood	68241	San Vicente	48509
418037730	Dorm	Cambridge	985	San Giuliano di Puglia	GD94 8GB
526757528	Office	Old Shore	5	Randazzo	327302
205464713	Dorm	Debra	75	Broken Hill	E6X 1G3
381265714	Dorm	Hudson	45741	Valleyview	319132
257070674	House	Morrow	8113	Panipat	31410
147162056	Apartment	Jana	2	Nanton	7952
231353044	Dorm	Jay	6	São João de Meriti	5838
130234473	Dorm	Lawn	5	Pietrarubbia	1837 IA
488803118	House	Ryan	449	Sioux City	28003
705058635	Dorm	Schlimgen	476	Rochester	73370
241171244	Office	Farmco	4405	Hengelo	20407
253553636	Dorm	Logan	10	Bolsward	909324
181741421	Dorm	Kedzie	46146	Ollagüe	48911059
687575673	Dorm	Petterle	78	Molenbeersel	3954
316053172	Apartment	Jay	6	Romano d'Ezzelino	246700
721670765	Office	Valley Edge	25878	Raiganj	13777
864324233	Dorm	Reindahl	650	Siedlce	54930
88665010	House	Carpenter	1644	La Ligua	7118 BN
217002248	Dorm	Green Ridge	984	Turrialba	74747
744427733	Apartment	Lerdahl	1	Shaki	11211
268300780	Office	Mallard	682	Lumaco	4127
316421848	Office	Chive	4196	MontigniesSaintChristophe	606788
61546133	House	Lawn	88	Neuss	3123
116187483	Office	Calypso	216	Pescantina	N08 6SM
251471352	Office	Oneill	43	La Thuile	10400
105711383	House	Hooker	2	Conca Casale	6376 UC
731720071	Dorm	Merchant	29	Oudegem	49878
375275174	House	Melody	5	Crescentino	87283
220676076	Dorm	Sage	7	SaintDenisBovesse	57676465
263281815	Dorm	Petterle	46	Matera	2826
86524244	Office	Merchant	44855	Mount Pearl	F6 4MR
552044235	Dorm	Summerview	2	Cariboo Regional District	503313
764747116	Dorm	Derek	992	Cuxhaven	R0K 9L7
268076132	House	Garrison	9746	Salem	3807
285653661	Dorm	Eagan	1	Neath	2584
806070614	Dorm	Nancy	81	Szczecin	PZ9M 3BN
658051512	Apartment	Loomis	436	Opgrimbie	3485
156177876	Dorm	Oxford	3251	Armidale	71913
522780470	Apartment	Spenser	98717	Crato	A5X 6E7
522780470	House	Anhalt	5728	El Tabo	38765
477801811	Office	Thompson	8	Caledon	8597 ZA
477801811	House	Northwestern	20638	Langenburg	55617
244405078	Apartment	Grasskamp	9	San Diego	638277
244405078	Office	Caliangt	3969	Canmore	5067
152737323	Offiice	Huxley	93	Lutterworth	15838
152737323	Dorm	Bobwhite	458	Sosnowiec	3743
608687105	House	Maple Wood	6758	Margate	61604
608687105	Apartment	Warrior	3423	Duisburg	9730
26306611	dorm	Linden	54	Trollhättan	L5X 9B8
26306611	House	Chive	9	Pietragalla	73203
373374216	House	Butternut	663	Esslingen	1838
373374216	dorm	Basil	9	Stevoort	80749
657567004	House	Mallory	7509	Outremont	46044
657567004	dorm	Chinook	56382	Pudahuel	8909
687150224	House	Haas	98	Sarre	43076
687150224	Apartment	Main	42	Ponta Grossa	98415
70627880	Office	Shelley	133	Conca Casale	2223
70627880	House	Knutson	71	Castelluccio Inferiore	9395
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.anggota (no_ktp, poin, level) FROM stdin;
268300780	539	Silver
316421848	801	Gold
61546133	280	Bronze
116187483	872	Gold
251471352	1177	Gold
105711383	473	Bronze
731720071	99	Platinum
375275174	1108	Gold
220676076	802	Gold
263281815	165	Bronze
86524244	467	Bronze
552044235	306	Bronze
764747116	749	Silver
268076132	383	Bronze
285653661	509	Silver
806070614	583	Silver
658051512	786	Silver
156177876	851	Gold
522780470	1172	Gold
65787275	1037	Gold
260351280	834	Gold
272253762	714	Silver
600737262	911	Gold
656374076	322	Bronze
832805316	435	Bronze
537106202	708	Silver
223650412	498	Bronze
741010433	298	Bronze
527034322	733	Silver
251367872	1092	Gold
766878206	318	Bronze
503723662	192	Bronze
314631664	235	Bronze
385158543	926	Gold
102250071	38	Platinum
844441486	613	Silver
47578854	312	Bronze
721342626	99	Platinum
683126206	286	Bronze
43218620	464	Bronze
283155418	821	Gold
353707750	204	Bronze
635325146	1006	Gold
278252278	1077	Gold
758864457	954	Gold
716231711	748	Silver
326388820	1190	Gold
203456425	400	Bronze
273327253	428	Bronze
115273830	573	Silver
35242664	99	Platinum
631128376	390	Bronze
607146165	269	Bronze
254150242	686	Silver
117303313	1142	Gold
82101555	763	Silver
456153051	830	Gold
642371487	798	Silver
75314253	395	Bronze
418037730	97	Platinum
526757528	685	Silver
205464713	775	Silver
381265714	572	Silver
257070674	165	Bronze
147162056	803	Gold
231353044	998	Gold
130234473	513	Silver
488803118	132	Bronze
705058635	541	Silver
241171244	1046	Gold
253553636	286	Bronze
181741421	253	Bronze
687575673	765	Silver
316053172	1183	Gold
721670765	1180	Gold
864324233	801	Gold
88665010	696	Silver
217002248	668	Silver
744427733	755	Silver
477801811	378	Bronze
244405078	1133	Gold
152737323	109	Bronze
608687105	280	Bronze
26306611	522	Silver
373374216	738	Silver
657567004	528	Silver
687150224	517	Silver
70627880	523	Silver
754822233	100	Bronze
531146331	-1	\N
\.


--
-- Data for Name: barang; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.barang (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa) FROM stdin;
597yIUSTYB	Mahjong	Yellow	http://dummyimage.com/122x175.bmp/cc0000/ffffff	baru	26	117303313
002ynxENls	Lego	Khaki	http://dummyimage.com/179x225.png/dddddd/000000	bagus	13	635325146
319wLHaxpA	Megablocks	Puce	http://dummyimage.com/239x109.jpg/5fa2dd/ffffff	bagus	27	385158543
980KnHzfrS	Robot Dog	Khaki	http://dummyimage.com/221x201.png/dddddd/000000	rusak kecil	20	263281815
381MiPpwTt	Chemistry Set	Fuscia	http://dummyimage.com/191x174.bmp/5fa2dd/ffffff	bagus	5	278252278
486rzsMYby	Mahjong	Goldenrod	http://dummyimage.com/146x116.jpg/ff4444/ffffff	rusak kecil	14	272253762
573pPAovsb	Activity Walker	Violet	http://dummyimage.com/222x116.bmp/ff4444/ffffff	rusak kecil	30	741010433
410dgyFNpn	Kaleidoskop	Mauv	http://dummyimage.com/110x242.bmp/dddddd/000000	baru	9	658051512
117PGcBSeH	Jumping Jack	Turquoise	http://dummyimage.com/229x206.bmp/dddddd/000000	rusak kecil	15	316053172
998akQArEI	Mahkota Raja	Blue	http://dummyimage.com/134x179.bmp/dddddd/000000	rusak kecil	15	503723662
576gpfuxNO	Megablocks	Fuscia	http://dummyimage.com/141x201.bmp/ff4444/ffffff	rusak kecil	17	608687105
996OkCGDBA	Nerf	Goldenrod	http://dummyimage.com/120x163.bmp/dddddd/000000	baru	24	657567004
853rOiDAhB	Rag Doll	Pink	http://dummyimage.com/126x241.bmp/5fa2dd/ffffff	baru	15	658051512
459ilEodpD	Mahjong	Yellow	http://dummyimage.com/182x162.jpg/dddddd/000000	rusak parah	19	61546133
587Ensivxp	Stroller Toy	Maroon	http://dummyimage.com/203x210.jpg/cc0000/ffffff	baru	25	656374076
042MlAbowZ	Mahjong	Yellow	http://dummyimage.com/175x213.jpg/5fa2dd/ffffff	baru	8	257070674
641CMkRhny	Water Gun	Turquoise	http://dummyimage.com/205x114.png/cc0000/ffffff	bagus	1	117303313
873XeWonSi	Digital pet	Turquoise	http://dummyimage.com/207x213.png/dddddd/000000	baru	15	766878206
771eXHsMtT	Puzzle Lantai	Teal	http://dummyimage.com/244x139.png/ff4444/ffffff	bagus	4	35242664
791ySsAIOn	Lego	Teal	http://dummyimage.com/227x139.png/5fa2dd/ffffff	baru	2	600737262
031cEpsVwA	Crayon	Turquoise	http://dummyimage.com/239x222.png/5fa2dd/ffffff	rusak parah	13	832805316
473OYljvzM	Lego	Khaki	http://dummyimage.com/249x230.jpg/cc0000/ffffff	bagus	5	716231711
170UyOCRmx	Digital pet	Maroon	http://dummyimage.com/199x178.bmp/dddddd/000000	bagus	17	531146331
523LFpiRPU	Robot Dog	Mauv	http://dummyimage.com/114x206.png/cc0000/ffffff	rusak parah	22	283155418
745jsLMVBZ	Rag Doll	Pink	http://dummyimage.com/142x238.bmp/ff4444/ffffff	rusak parah	18	607146165
378TDOxqHw	Puzzle Lantai	Indigo	http://dummyimage.com/109x173.png/ff4444/ffffff	baru	6	220676076
790foFmAdH	Crayon	Yellow	http://dummyimage.com/182x245.jpg/ff4444/ffffff	rusak parah	24	244405078
703zMkAHZF	Robot Dog	Goldenrod	http://dummyimage.com/233x148.png/5fa2dd/ffffff	rusak kecil	10	316421848
006pvFlcnb	Baby Maracas	Fuscia	http://dummyimage.com/164x224.jpg/ff4444/ffffff	bagus	6	257070674
767JqzdrDE	Jigsaw	Yellow	http://dummyimage.com/152x244.png/cc0000/ffffff	rusak kecil	21	764747116
497olytRzk	Mahkota Raja	Aquamarine	http://dummyimage.com/163x193.bmp/cc0000/ffffff	baru	20	326388820
961tiYnGxD	Stroller Toy	Goldenrod	http://dummyimage.com/209x118.bmp/ff4444/ffffff	bagus	19	687575673
913kwHRxzK	Puzzle Lantai	Pink	http://dummyimage.com/205x210.jpg/dddddd/000000	baru	25	117303313
603DwSpnyF	Electric Baby Swinger	Purple	http://dummyimage.com/190x154.png/dddddd/000000	rusak kecil	5	260351280
015XczoKAy	Mahjong	Goldenrod	http://dummyimage.com/168x242.jpg/dddddd/000000	rusak parah	6	268076132
965CfOpIKt	Teddy Bear	Red	http://dummyimage.com/236x227.bmp/dddddd/000000	baru	4	130234473
954XCdRkUB	Lego	Violet	http://dummyimage.com/190x243.jpg/ff4444/ffffff	rusak parah	19	220676076
164KgxQmWe	Water Gun	Orange	http://dummyimage.com/204x168.png/dddddd/000000	baru	5	260351280
159LQCdtDg	Crayon	Violet	http://dummyimage.com/208x216.png/5fa2dd/ffffff	baru	20	537106202
591WdwYSNM	Baby Maracas	Crimson	http://dummyimage.com/122x101.bmp/dddddd/000000	rusak kecil	6	65787275
247GszTkqv	Water Gun	Pink	http://dummyimage.com/151x132.png/dddddd/000000	rusak kecil	10	316053172
476qpOVNbQ	Drawing Book	Aquamarine	http://dummyimage.com/133x184.bmp/dddddd/000000	rusak kecil	24	687150224
533CwpzDcn	Crayon	Fuscia	http://dummyimage.com/120x126.jpg/ff4444/ffffff	bagus	13	716231711
162zhNGyUQ	Mahkota Raja	Teal	http://dummyimage.com/181x217.png/5fa2dd/ffffff	rusak kecil	28	522780470
943ZQYlvFB	Stroller Toy	Pink	http://dummyimage.com/115x153.bmp/cc0000/ffffff	bagus	5	531146331
851ZlQqsKB	Robot Dog	Goldenrod	http://dummyimage.com/108x169.jpg/ff4444/ffffff	baru	20	537106202
933UCaJpBM	Chemistry Set	Aquamarine	http://dummyimage.com/145x120.jpg/ff4444/ffffff	baru	4	115273830
068mHhZkxE	Jigsaw	Maroon	http://dummyimage.com/145x180.png/cc0000/ffffff	rusak parah	3	253553636
748TpJvbnk	Chemistry Set	Blue	http://dummyimage.com/228x113.bmp/ff4444/ffffff	rusak parah	4	82101555
747gwOGnUX	Robot Dog	Goldenrod	http://dummyimage.com/185x127.jpg/cc0000/ffffff	rusak kecil	26	88665010
045Cbeswxq	KNex	Maroon	http://dummyimage.com/110x220.png/cc0000/ffffff	rusak kecil	30	117303313
967EVfvcKy	Rag Doll	Goldenrod	http://dummyimage.com/213x204.png/ff4444/ffffff	bagus	12	61546133
548rxhygjQ	Mahjong	Puce	http://dummyimage.com/186x115.jpg/dddddd/000000	bagus	29	766878206
468sVAhOpW	Teddy Bear	Turquoise	http://dummyimage.com/188x213.bmp/dddddd/000000	rusak parah	26	35242664
621GScFZKL	Stroller Toy	Violet	http://dummyimage.com/101x219.bmp/cc0000/ffffff	baru	25	205464713
236EAkLbDm	Mahjong	Puce	http://dummyimage.com/132x247.png/ff4444/ffffff	baru	28	687150224
531NftuHay	Mahjong	Crimson	http://dummyimage.com/161x181.png/5fa2dd/ffffff	bagus	25	147162056
071qAWNmBv	Megablocks	Goldenrod	http://dummyimage.com/218x250.bmp/dddddd/000000	rusak kecil	1	130234473
932hQZBCKP	Digital pet	Green	http://dummyimage.com/106x203.png/cc0000/ffffff	baru	2	75314253
957UYufSzN	Megablocks	Blue	http://dummyimage.com/200x194.png/cc0000/ffffff	sedang disewa anggota	5	316421848
877dHkyXxu	Robot Dog	Blue	http://dummyimage.com/107x231.png/dddddd/000000	baru	1	721670765
605amnjwSI	Hot Wheels Car	Maroon	http://dummyimage.com/245x142.jpg/dddddd/000000	bagus	30	642371487
354OZlwDpU	Crayon	Teal	http://dummyimage.com/171x133.bmp/dddddd/000000	rusak kecil	23	456153051
339aXSZGbu	Megablocks	Fuscia	http://dummyimage.com/149x195.png/ff4444/ffffff	rusak parah	6	43218620
540PVGskHT	Stroller Toy	Green	http://dummyimage.com/200x101.png/ff4444/ffffff	bagus	5	257070674
234oPZXKYA	Lego	Blue	http://dummyimage.com/243x172.jpg/5fa2dd/ffffff	rusak parah	28	608687105
887GAXaCLU	Kaleidoskop	Teal	http://dummyimage.com/128x214.bmp/ff4444/ffffff	rusak parah	30	721342626
376fCoElRQ	Drawing Book	Indigo	http://dummyimage.com/138x155.jpg/5fa2dd/ffffff	rusak kecil	23	832805316
113fmbPTkB	Activity Walker	Turquoise	http://dummyimage.com/119x240.png/dddddd/000000	baru	8	314631664
932VDZpgnr	Crayon	Crimson	http://dummyimage.com/243x244.jpg/ff4444/ffffff	rusak parah	17	26306611
607rKyDwjL	Mahkota Raja	Fuscia	http://dummyimage.com/124x157.jpg/ff4444/ffffff	rusak kecil	15	272253762
642CaOYGQz	Activity Walker	Blue	http://dummyimage.com/217x119.bmp/dddddd/000000	rusak kecil	7	687150224
189YPWiJqf	Hot Wheels Car	Goldenrod	http://dummyimage.com/113x134.bmp/5fa2dd/ffffff	bagus	25	241171244
364HcZDUxl	Hot Wheels Car	Pink	http://dummyimage.com/212x179.bmp/dddddd/000000	bagus	29	241171244
489SLntDIO	Hot Wheels Car	Teal	http://dummyimage.com/112x114.jpg/dddddd/000000	bagus	12	864324233
269uIJzqOE	Activity Walker	Yellow	http://dummyimage.com/205x199.bmp/cc0000/ffffff	baru	8	285653661
334ghumliD	Crayon	Violet	http://dummyimage.com/221x199.jpg/5fa2dd/ffffff	rusak kecil	1	253553636
796yvHCJuc	Activity Walker	Yellow	http://dummyimage.com/201x228.jpg/cc0000/ffffff	baru	5	105711383
989sbAInxW	Baby Maracas	Turquoise	http://dummyimage.com/216x170.bmp/ff4444/ffffff	rusak parah	4	705058635
669hvoEBYF	Mahjong	Goldenrod	http://dummyimage.com/202x242.png/5fa2dd/ffffff	rusak parah	15	326388820
603tnsafeB	Hot Wheels Car	Red	http://dummyimage.com/111x202.png/ff4444/ffffff	rusak kecil	29	260351280
700PJaNdLC	Hot Wheels Car	Mauv	http://dummyimage.com/140x150.png/ff4444/ffffff	baru	17	531146331
519akCvcKT	Mahjong	Turquoise	http://dummyimage.com/218x209.png/dddddd/000000	rusak parah	8	43218620
771FSxYJzB	Stroller Toy	Orange	http://dummyimage.com/201x241.jpg/5fa2dd/ffffff	bagus	25	47578854
240iOUyYHo	Kaleidoskop	Teal	http://dummyimage.com/218x238.png/dddddd/000000	bagus	19	552044235
866Xrynjmp	Digital pet	Orange	http://dummyimage.com/113x177.png/5fa2dd/ffffff	bagus	23	251367872
019uSgpOWD	Mahjong	Turquoise	http://dummyimage.com/172x199.jpg/ff4444/ffffff	rusak parah	22	278252278
011TYnmbcG	B-Daman	Maroon	http://dummyimage.com/215x248.jpg/dddddd/000000	rusak parah	15	488803118
983mXwuMlh	Jumping Jack	Pink	http://dummyimage.com/180x200.png/ff4444/ffffff	bagus	10	385158543
411SYiLmZN	Nerf	Crimson	http://dummyimage.com/171x151.bmp/ff4444/ffffff	rusak parah	5	82101555
738LzeQtqi	Crayon	Aquamarine	http://dummyimage.com/198x145.jpg/ff4444/ffffff	bagus	17	268076132
005pjEdctb	Water Gun	Maroon	http://dummyimage.com/233x174.bmp/cc0000/ffffff	bagus	5	373374216
261QLCUzGR	Puzzle Lantai	Orange	http://dummyimage.com/191x131.jpg/cc0000/ffffff	rusak parah	27	223650412
669XOQlRzY	KNex	Teal	http://dummyimage.com/220x140.jpg/cc0000/ffffff	baru	13	844441486
563nOpuQhE	Megablocks	Violet	http://dummyimage.com/248x100.png/5fa2dd/ffffff	baru	4	205464713
631zwMrPAW	Water Gun	Fuscia	http://dummyimage.com/220x168.png/dddddd/000000	baru	6	257070674
051qZPRamN	Teddy Bear	Teal	http://dummyimage.com/221x184.bmp/cc0000/ffffff	sedang disewa anggota	25	373374216
870NokMalA	Kaleidoskop	Indigo	http://dummyimage.com/137x149.jpg/5fa2dd/ffffff	sedang disewa anggota	21	156177876
593EFynrSv	Activity Walker	Khaki	http://dummyimage.com/104x217.bmp/5fa2dd/ffffff	sedang disewa anggota	10	147162056
530ZYTDplq	Baby Maracas	Indigo	http://dummyimage.com/144x238.png/cc0000/ffffff	sedang disewa anggota	5	75314253
\.


--
-- Data for Name: barang_dikembalikan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.barang_dikembalikan (no_resi, no_urut, id_barang) FROM stdin;
100001	1	597yIUSTYB
100002	2	002ynxENls
100003	3	319wLHaxpA
100004	4	980KnHzfrS
100005	5	381MiPpwTt
100006	6	486rzsMYby
100007	7	957UYufSzN
100008	8	573pPAovsb
100009	9	410dgyFNpn
100010	10	117PGcBSeH
100011	11	998akQArEI
100012	12	576gpfuxNO
100013	13	996OkCGDBA
100014	14	853rOiDAhB
100015	15	459ilEodpD
100016	16	587Ensivxp
100017	17	042MlAbowZ
100018	18	641CMkRhny
100019	19	873XeWonSi
100020	20	771eXHsMtT
100001	21	791ySsAIOn
100002	22	031cEpsVwA
100003	23	051qZPRamN
100004	24	473OYljvzM
100005	25	170UyOCRmx
100006	26	523LFpiRPU
100007	27	745jsLMVBZ
100008	28	378TDOxqHw
100009	29	790foFmAdH
100010	30	703zMkAHZF
100011	31	006pvFlcnb
100012	32	767JqzdrDE
100013	33	497olytRzk
100014	34	961tiYnGxD
100015	35	913kwHRxzK
100016	36	603DwSpnyF
100017	37	015XczoKAy
100018	38	965CfOpIKt
100019	39	954XCdRkUB
100020	40	164KgxQmWe
\.


--
-- Data for Name: barang_dikirim; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.barang_dikirim (no_resi, no_urut, id_barang, tanggal_review, review) FROM stdin;
100014	1	540PVGskHT	2018-06-07	loh cuman ada kardusnya
100014	2	605amnjwSI	2018-10-23	biasa aja
100005	3	790foFmAdH	2018-08-25	mantab gan
100006	4	603DwSpnyF	2019-02-01	mantab gan
100012	5	747gwOGnUX	2018-04-28	biasa aja
100008	6	913kwHRxzK	2018-12-02	bagus
100002	7	523LFpiRPU	2019-01-03	oke oce
100012	8	576gpfuxNO	2018-06-24	loh cuman ada kardusnya
100006	9	873XeWonSi	2018-09-23	loh cuman ada kardusnya
100006	10	269uIJzqOE	2018-09-29	bagus
100004	11	519akCvcKT	2018-09-06	loh cuman ada kardusnya
100006	12	591WdwYSNM	2019-02-03	biasa aja
100016	13	240iOUyYHo	2018-08-16	mantab gan
100009	14	576gpfuxNO	2018-10-18	oke oce
100006	15	771FSxYJzB	2018-07-04	biasa aja
100003	16	980KnHzfrS	2018-11-30	bagus
100018	17	411SYiLmZN	2018-10-10	mantab gan
100005	18	998akQArEI	2018-10-25	bagus
100016	19	877dHkyXxu	2018-12-24	bagus
100007	20	563nOpuQhE	2018-11-02	oke oce
100008	21	378TDOxqHw	2018-10-11	biasa aja
100004	22	887GAXaCLU	2018-05-31	biasa aja
100012	23	533CwpzDcn	2018-07-27	bagus
100009	24	113fmbPTkB	2019-03-20	bagus
100013	25	747gwOGnUX	2018-07-12	biasa aja
100012	26	339aXSZGbu	2018-09-19	mantab gan
100011	27	170UyOCRmx	2019-03-29	oke oce
100010	28	791ySsAIOn	2019-03-20	biasa aja
100015	29	603tnsafeB	2018-10-24	biasa aja
100013	30	410dgyFNpn	2018-06-26	bagus
100010	31	117PGcBSeH	2019-02-24	biasa aja
100018	32	576gpfuxNO	2019-02-09	oke oce
100018	33	378TDOxqHw	2018-06-06	loh cuman ada kardusnya
100016	34	378TDOxqHw	2018-09-05	mantab gan
100016	35	240iOUyYHo	2018-04-19	loh cuman ada kardusnya
100018	36	870NokMalA	2018-05-22	oke oce
100014	37	236EAkLbDm	2018-04-26	mantab gan
100012	38	877dHkyXxu	2019-01-14	mantab gan
100014	39	468sVAhOpW	2019-02-13	mantab gan
100002	40	339aXSZGbu	2019-01-29	loh cuman ada kardusnya
\.


--
-- Data for Name: barang_pesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.barang_pesanan (id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, tanggal_kembali, status) FROM stdin;
24	1	051qZPRamN	2019-01-23	9	\N	SEDANG_DISIAPKAN
5	2	642CaOYGQz	2019-01-18	6	\N	SEDANG_DISIAPKAN
44	3	853rOiDAhB	2018-09-12	2	\N	SEDANG_DIKONFIRMASI
35	4	563nOpuQhE	2018-11-28	13	\N	SEDANG_DIKONFIRMASI
15	5	489SLntDIO	2018-07-01	14	\N	MENUNGGU_PEMBAYARAN
17	6	540PVGskHT	2018-10-12	12	\N	MENUNGGU_PEMBAYARAN
37	7	473OYljvzM	2018-05-27	7	\N	DALAM_MASA_SEWA
38	8	597yIUSTYB	2018-12-13	4	\N	SEDANG_DISIAPKAN
31	9	476qpOVNbQ	2019-02-03	9	\N	SEDANG_DIKIRIM
27	10	738LzeQtqi	2018-09-28	9	\N	SEDANG_DIKIRIM
38	11	998akQArEI	2019-02-28	11	\N	MENUNGGU_PEMBAYARAN
48	12	319wLHaxpA	2019-02-20	12	\N	BATAL
23	13	519akCvcKT	2018-07-13	3	2018-07-16	SUDAH_DIKEMBALIKAN
4	14	486rzsMYby	2018-07-21	8	\N	BATAL
22	15	669XOQlRzY	2018-12-17	12	2018-12-29	SUDAH_DIKEMBALIKAN
18	16	967EVfvcKy	2018-04-19	7	\N	MENUNGGU_PEMBAYARAN
4	17	164KgxQmWe	2019-01-26	10	\N	BATAL
10	18	967EVfvcKy	2018-12-05	9	\N	SEDANG_DISIAPKAN
21	19	932VDZpgnr	2018-06-30	7	\N	SEDANG_DIKIRIM
45	20	996OkCGDBA	2018-06-30	13	\N	SEDANG_DIKONFIRMASI
34	21	459ilEodpD	2018-11-10	6	2018-11-16	SUDAH_DIKEMBALIKAN
2	22	593EFynrSv	2018-12-11	5	\N	BATAL
28	23	771eXHsMtT	2019-02-15	11	\N	SEDANG_DISIAPKAN
13	24	771FSxYJzB	2018-10-24	4	\N	DALAM_MASA_SEWA
45	25	015XczoKAy	2019-03-27	9	\N	SEDANG_DIKONFIRMASI
1	26	523LFpiRPU	2018-12-24	11	\N	SEDANG_DIKONFIRMASI
11	27	045Cbeswxq	2018-12-19	2	\N	SEDANG_DISIAPKAN
28	28	476qpOVNbQ	2018-09-13	8	\N	SEDANG_DISIAPKAN
9	29	703zMkAHZF	2018-05-19	14	\N	BATAL
19	30	593EFynrSv	2018-04-23	9	\N	SEDANG_DIKIRIM
21	31	476qpOVNbQ	2018-09-17	6	\N	DALAM_MASA_SEWA
48	32	747gwOGnUX	2019-01-16	6	\N	SEDANG_DISIAPKAN
6	33	269uIJzqOE	2018-08-07	14	\N	DALAM_MASA_SEWA
27	34	006pvFlcnb	2019-03-29	4	2019-04-02	SUDAH_DIKEMBALIKAN
49	35	410dgyFNpn	2018-06-24	10	\N	SEDANG_DIKIRIM
2	36	051qZPRamN	2018-09-28	3	\N	SEDANG_DIKONFIRMASI
25	37	031cEpsVwA	2018-08-11	5	\N	SEDANG_DISIAPKAN
40	38	005pjEdctb	2019-03-25	8	\N	SEDANG_DIKONFIRMASI
47	39	530ZYTDplq	2018-12-03	8	\N	MENUNGGU_PEMBAYARAN
14	40	605amnjwSI	2019-03-01	2	\N	SEDANG_DISIAPKAN
4	41	236EAkLbDm	2018-07-20	6	2018-07-26	SUDAH_DIKEMBALIKAN
11	42	961tiYnGxD	2018-07-25	4	\N	SEDANG_DIKIRIM
33	43	002ynxENls	2018-06-19	9	\N	SEDANG_DIKIRIM
44	44	603DwSpnyF	2019-03-11	8	\N	SEDANG_DISIAPKAN
24	45	603tnsafeB	2018-11-25	3	2018-11-28	SUDAH_DIKEMBALIKAN
11	46	870NokMalA	2018-11-06	7	\N	BATAL
31	47	996OkCGDBA	2019-03-05	3	\N	DALAM_MASA_SEWA
42	48	933UCaJpBM	2019-01-13	13	\N	DALAM_MASA_SEWA
30	49	533CwpzDcn	2019-02-22	10	\N	MENUNGGU_PEMBAYARAN
17	50	354OZlwDpU	2018-05-02	7	\N	BATAL
24	51	700PJaNdLC	2018-07-14	2	\N	SEDANG_DISIAPKAN
41	52	957UYufSzN	2018-04-19	13	\N	BATAL
25	53	887GAXaCLU	2018-08-08	14	\N	MENUNGGU_PEMBAYARAN
27	54	319wLHaxpA	2018-06-27	8	2018-07-05	SUDAH_DIKEMBALIKAN
30	55	410dgyFNpn	2019-01-12	5	\N	BATAL
25	56	005pjEdctb	2018-11-25	9	\N	SEDANG_DIKONFIRMASI
4	57	381MiPpwTt	2018-08-06	3	\N	SEDANG_DIKONFIRMASI
40	58	261QLCUzGR	2018-08-05	5	\N	DALAM_MASA_SEWA
48	59	473OYljvzM	2018-05-01	1	\N	SEDANG_DIKIRIM
8	60	913kwHRxzK	2018-05-07	10	\N	SEDANG_DIKIRIM
39	61	240iOUyYHo	2019-01-27	5	\N	SEDANG_DISIAPKAN
32	62	486rzsMYby	2018-09-30	14	\N	SEDANG_DISIAPKAN
1	63	747gwOGnUX	2018-04-28	4	\N	SEDANG_DIKONFIRMASI
28	64	771FSxYJzB	2018-07-19	11	\N	SEDANG_DIKIRIM
16	65	005pjEdctb	2018-05-07	2	2018-05-09	SUDAH_DIKEMBALIKAN
30	66	576gpfuxNO	2019-03-09	12	\N	DALAM_MASA_SEWA
33	67	642CaOYGQz	2018-07-09	10	\N	BATAL
5	68	866Xrynjmp	2019-02-19	7	\N	DALAM_MASA_SEWA
45	69	240iOUyYHo	2019-03-21	9	\N	SEDANG_DIKONFIRMASI
19	70	669XOQlRzY	2018-04-19	5	\N	SEDANG_DIKIRIM
8	71	621GScFZKL	2018-08-03	11	\N	SEDANG_DIKONFIRMASI
3	72	261QLCUzGR	2018-08-02	3	\N	MENUNGGU_PEMBAYARAN
46	73	002ynxENls	2018-06-29	12	2018-07-11	SUDAH_DIKEMBALIKAN
29	74	943ZQYlvFB	2018-09-13	13	\N	SEDANG_DIKIRIM
25	75	621GScFZKL	2018-11-29	11	\N	SEDANG_DIKIRIM
2	76	870NokMalA	2018-06-15	4	\N	SEDANG_DISIAPKAN
37	77	247GszTkqv	2019-02-15	6	\N	SEDANG_DIKIRIM
17	78	873XeWonSi	2018-12-18	2	\N	BATAL
40	79	851ZlQqsKB	2018-12-19	10	\N	SEDANG_DISIAPKAN
24	80	998akQArEI	2018-07-15	12	\N	SEDANG_DIKONFIRMASI
44	81	853rOiDAhB	2018-05-17	4	\N	BATAL
40	82	669hvoEBYF	2018-08-06	3	\N	SEDANG_DIKIRIM
24	83	486rzsMYby	2019-03-29	7	\N	DALAM_MASA_SEWA
42	84	247GszTkqv	2018-11-24	10	\N	SEDANG_DIKIRIM
29	85	965CfOpIKt	2019-03-15	8	\N	SEDANG_DIKONFIRMASI
28	86	771FSxYJzB	2019-02-24	8	\N	SEDANG_DISIAPKAN
35	87	236EAkLbDm	2019-03-07	7	\N	BATAL
3	88	669XOQlRzY	2018-07-17	6	\N	DALAM_MASA_SEWA
19	89	700PJaNdLC	2018-08-26	10	\N	BATAL
13	90	957UYufSzN	2018-10-27	9	\N	DALAM_MASA_SEWA
32	91	489SLntDIO	2018-10-31	10	\N	MENUNGGU_PEMBAYARAN
21	92	162zhNGyUQ	2018-09-10	3	\N	MENUNGGU_PEMBAYARAN
29	93	603DwSpnyF	2018-11-22	12	\N	SEDANG_DISIAPKAN
11	94	603DwSpnyF	2019-03-23	1	2019-03-24	SUDAH_DIKEMBALIKAN
11	95	533CwpzDcn	2018-08-30	8	\N	SEDANG_DIKIRIM
39	96	540PVGskHT	2018-06-07	8	\N	SEDANG_DIKONFIRMASI
17	97	031cEpsVwA	2018-10-26	6	\N	SEDANG_DIKIRIM
20	98	531NftuHay	2018-09-20	4	\N	MENUNGGU_PEMBAYARAN
16	99	996OkCGDBA	2019-04-09	8	\N	SEDANG_DIKONFIRMASI
48	100	998akQArEI	2019-03-27	12	\N	SEDANG_DISIAPKAN
47	101	957UYufSzN	2020-01-03	17	\N	SEDANG_DIKONFIRMASI
2	101	957UYufSzN	2020-01-03	17	\N	SEDANG_DIKONFIRMASI
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.chat (id, pesan, date_time, no_ktp_anggota, no_ktp_admin) FROM stdin;
1	Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.	2018-11-17 10:16:24	488803118	102287558
2	Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.	2018-05-18 14:12:29	220676076	12657044
3	Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.	2017-08-22 11:29:36	316053172	606335605
4	Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.	2017-12-26 13:29:02	241171244	181044377
5	Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.	2017-09-11 16:06:37	731720071	723132618
6	Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.	2017-10-05 04:21:04	205464713	102287558
7	Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.	2019-01-21 01:24:58	61546133	143882080
8	Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.	2018-09-05 20:18:18	285653661	606335605
9	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.	2019-01-09 11:07:16	205464713	102287558
10	Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.	2018-08-13 01:18:45	716231711	667334564
11	Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.	2018-12-05 05:13:00	744427733	12657044
12	Fusce consequat. Nulla nisl. Nunc nisl.	2017-11-08 17:06:12	527034322	12657044
13	Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.	2018-12-17 06:41:01	766878206	667334564
14	Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.	2017-11-03 03:49:48	205464713	143882080
15	Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.	2017-08-06 19:10:41	43218620	181044377
16	Sed ante. Vivamus tortor. Duis mattis egestas metus.	2018-10-03 10:20:30	552044235	143882080
17	Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.	2019-01-12 02:33:43	61546133	181044377
18	Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.	2019-03-14 04:48:03	477801811	707580576
19	Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.	2018-11-12 08:56:43	631128376	12657044
20	Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.	2019-02-19 22:21:16	716231711	143882080
21	Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.	2017-10-23 16:42:27	223650412	12657044
22	Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.	2018-08-05 08:22:11	687150224	181044377
23	Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.	2017-04-28 00:42:13	353707750	667334564
24	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.	2017-07-19 03:42:16	117303313	606335605
25	Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.	2017-04-17 03:58:59	766878206	707580576
26	Phasellus in felis. Donec semper sapien a libero. Nam dui.	2017-09-01 08:50:31	285653661	181044377
27	Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.	2017-10-06 08:53:01	642371487	102287558
28	Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.	2017-09-28 18:55:27	477801811	476641275
29	Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.	2017-11-29 05:39:10	75314253	476641275
30	In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.	2018-09-11 10:28:43	381265714	846603383
31	Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.	2018-04-14 10:55:16	600737262	667334564
32	Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.	2017-05-22 06:34:33	552044235	606335605
33	Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.	2018-12-02 20:40:45	65787275	476641275
34	In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.	2017-06-21 16:50:47	705058635	846603383
35	Sed ante. Vivamus tortor. Duis mattis egestas metus.	2018-07-13 13:47:33	115273830	143882080
36	Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.	2018-04-09 04:18:12	254150242	846603383
37	Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.	2017-06-10 05:53:52	716231711	12657044
38	Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.	2019-04-03 08:07:59	156177876	723132618
39	Fusce consequat. Nulla nisl. Nunc nisl.	2018-10-22 21:20:09	488803118	12657044
40	Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.	2017-10-20 18:27:07	522780470	707580576
41	Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.	2017-08-12 23:33:45	608687105	846603383
42	Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.	2018-04-03 09:15:03	683126206	707580576
43	Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.	2018-03-11 20:26:28	105711383	143882080
44	Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.	2018-06-24 11:05:52	115273830	143882080
45	Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.	2017-12-23 09:43:20	223650412	707580576
46	Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.	2017-05-25 14:34:35	381265714	707580576
47	Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.	2019-02-09 09:29:07	608687105	12657044
48	In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.	2017-11-01 08:52:53	86524244	606335605
49	Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.	2018-11-18 07:45:10	806070614	102287558
50	Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.	2017-06-16 14:16:37	721342626	143882080
\.


--
-- Data for Name: info_barang_level; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.info_barang_level (id_barang, nama_level, harga_sewa, porsi_loyalti) FROM stdin;
597yIUSTYB	Platinum	78339	2
002ynxENls	Platinum	48467	12
319wLHaxpA	Platinum	43770	16
980KnHzfrS	Platinum	33566	19
381MiPpwTt	Platinum	54055	9
486rzsMYby	Platinum	62504	1
957UYufSzN	Platinum	37999	3
573pPAovsb	Platinum	25260	1
410dgyFNpn	Platinum	47307	5
117PGcBSeH	Platinum	97120	14
998akQArEI	Platinum	85729	1
576gpfuxNO	Platinum	29488	20
996OkCGDBA	Platinum	27486	7
853rOiDAhB	Platinum	65433	4
459ilEodpD	Platinum	31026	8
587Ensivxp	Platinum	66485	2
042MlAbowZ	Platinum	47277	4
641CMkRhny	Platinum	47008	18
873XeWonSi	Platinum	30812	13
771eXHsMtT	Platinum	34459	19
791ySsAIOn	Platinum	75733	7
031cEpsVwA	Platinum	74773	20
051qZPRamN	Platinum	28742	4
473OYljvzM	Platinum	69022	13
170UyOCRmx	Platinum	91662	14
523LFpiRPU	Platinum	62273	10
745jsLMVBZ	Platinum	93740	17
378TDOxqHw	Platinum	44180	15
790foFmAdH	Platinum	34480	12
703zMkAHZF	Platinum	70685	1
006pvFlcnb	Platinum	23517	11
767JqzdrDE	Platinum	76529	2
497olytRzk	Platinum	99220	12
961tiYnGxD	Platinum	49484	15
913kwHRxzK	Platinum	52384	1
603DwSpnyF	Platinum	73037	6
015XczoKAy	Platinum	24064	5
965CfOpIKt	Platinum	68434	19
954XCdRkUB	Platinum	76061	12
164KgxQmWe	Platinum	94232	1
159LQCdtDg	Platinum	40193	15
591WdwYSNM	Platinum	96077	1
247GszTkqv	Platinum	52389	6
476qpOVNbQ	Platinum	52661	13
533CwpzDcn	Platinum	70385	1
162zhNGyUQ	Platinum	87490	7
943ZQYlvFB	Platinum	52665	10
851ZlQqsKB	Platinum	10355	17
530ZYTDplq	Platinum	15196	18
933UCaJpBM	Platinum	12449	13
068mHhZkxE	Platinum	73538	5
748TpJvbnk	Platinum	48099	9
747gwOGnUX	Platinum	74030	16
045Cbeswxq	Platinum	71188	6
967EVfvcKy	Platinum	89599	11
548rxhygjQ	Platinum	44072	16
468sVAhOpW	Platinum	19643	19
621GScFZKL	Platinum	23382	18
236EAkLbDm	Platinum	37834	4
531NftuHay	Platinum	51952	12
071qAWNmBv	Platinum	83340	20
932hQZBCKP	Platinum	66410	9
877dHkyXxu	Platinum	11345	17
605amnjwSI	Platinum	91202	6
870NokMalA	Platinum	58485	18
354OZlwDpU	Platinum	62192	13
339aXSZGbu	Platinum	44349	4
540PVGskHT	Platinum	31920	4
234oPZXKYA	Platinum	31731	6
887GAXaCLU	Platinum	88908	11
376fCoElRQ	Platinum	46914	16
113fmbPTkB	Platinum	62395	17
932VDZpgnr	Platinum	16257	6
607rKyDwjL	Platinum	20067	9
642CaOYGQz	Platinum	86171	8
189YPWiJqf	Platinum	29190	3
364HcZDUxl	Platinum	17315	12
593EFynrSv	Platinum	43502	6
489SLntDIO	Platinum	22100	20
269uIJzqOE	Platinum	47649	5
334ghumliD	Platinum	75804	11
796yvHCJuc	Platinum	85636	9
989sbAInxW	Platinum	54365	19
669hvoEBYF	Platinum	61599	5
603tnsafeB	Gold	66743	12
700PJaNdLC	Gold	40620	15
519akCvcKT	Gold	72893	8
771FSxYJzB	Gold	54265	17
240iOUyYHo	Gold	79416	13
866Xrynjmp	Gold	55904	2
019uSgpOWD	Gold	86527	10
011TYnmbcG	Gold	51894	3
983mXwuMlh	Gold	60353	9
411SYiLmZN	Gold	98422	18
738LzeQtqi	Gold	74998	14
005pjEdctb	Gold	93646	1
261QLCUzGR	Gold	24335	5
669XOQlRzY	Gold	45305	3
563nOpuQhE	Gold	82082	8
631zwMrPAW	Gold	46174	10
597yIUSTYB	Gold	66694	18
002ynxENls	Gold	38754	11
319wLHaxpA	Gold	64189	3
980KnHzfrS	Gold	79472	20
381MiPpwTt	Gold	80381	9
486rzsMYby	Gold	68676	9
957UYufSzN	Gold	95588	10
573pPAovsb	Gold	84334	5
410dgyFNpn	Gold	39893	10
117PGcBSeH	Gold	39881	20
998akQArEI	Gold	95481	6
576gpfuxNO	Gold	64175	3
996OkCGDBA	Gold	35640	4
853rOiDAhB	Gold	46798	19
459ilEodpD	Gold	38798	20
587Ensivxp	Gold	54904	17
042MlAbowZ	Gold	33311	18
641CMkRhny	Gold	74998	5
873XeWonSi	Gold	72169	14
771eXHsMtT	Gold	60104	18
791ySsAIOn	Gold	67960	20
031cEpsVwA	Gold	58852	9
051qZPRamN	Gold	51024	8
473OYljvzM	Gold	38192	5
170UyOCRmx	Gold	25873	16
523LFpiRPU	Gold	40844	8
745jsLMVBZ	Gold	84286	7
378TDOxqHw	Gold	25929	5
790foFmAdH	Gold	66819	15
703zMkAHZF	Gold	40831	16
006pvFlcnb	Gold	85900	1
767JqzdrDE	Gold	68296	14
497olytRzk	Gold	87853	8
961tiYnGxD	Gold	76130	19
913kwHRxzK	Gold	54571	10
603DwSpnyF	Gold	25232	8
015XczoKAy	Gold	68111	8
965CfOpIKt	Gold	49244	6
954XCdRkUB	Gold	87715	10
164KgxQmWe	Gold	29377	15
159LQCdtDg	Gold	42533	17
591WdwYSNM	Gold	17462	12
247GszTkqv	Gold	57423	19
476qpOVNbQ	Gold	69393	20
533CwpzDcn	Gold	71605	8
162zhNGyUQ	Gold	17175	16
943ZQYlvFB	Gold	39281	17
851ZlQqsKB	Gold	17237	6
530ZYTDplq	Gold	46669	7
933UCaJpBM	Gold	70822	15
068mHhZkxE	Gold	88368	12
748TpJvbnk	Gold	94319	20
747gwOGnUX	Gold	12810	14
045Cbeswxq	Gold	45717	18
967EVfvcKy	Gold	70175	7
548rxhygjQ	Gold	37081	16
468sVAhOpW	Gold	31749	14
621GScFZKL	Gold	96253	15
236EAkLbDm	Gold	13720	12
531NftuHay	Gold	24863	18
071qAWNmBv	Gold	23946	19
932hQZBCKP	Gold	15398	4
877dHkyXxu	Gold	59128	9
605amnjwSI	Gold	79941	6
870NokMalA	Gold	59681	17
354OZlwDpU	Gold	86847	15
339aXSZGbu	Gold	86121	14
540PVGskHT	Gold	66596	2
234oPZXKYA	Gold	71405	2
887GAXaCLU	Gold	73676	11
376fCoElRQ	Gold	61196	9
113fmbPTkB	Gold	43209	8
932VDZpgnr	Gold	83914	16
607rKyDwjL	Gold	55699	5
642CaOYGQz	Gold	35488	13
189YPWiJqf	Gold	62728	7
364HcZDUxl	Gold	89983	11
593EFynrSv	Gold	61248	16
489SLntDIO	Gold	51662	5
269uIJzqOE	Gold	17769	11
334ghumliD	Gold	48070	15
796yvHCJuc	Gold	54531	11
989sbAInxW	Silver	53418	13
669hvoEBYF	Silver	27665	8
603tnsafeB	Silver	14791	19
700PJaNdLC	Silver	27986	11
519akCvcKT	Silver	24225	16
771FSxYJzB	Silver	88854	9
240iOUyYHo	Silver	16963	4
866Xrynjmp	Silver	87004	3
019uSgpOWD	Silver	14074	17
011TYnmbcG	Silver	50671	10
983mXwuMlh	Silver	56433	15
411SYiLmZN	Silver	47100	6
738LzeQtqi	Silver	52372	19
005pjEdctb	Silver	16576	8
261QLCUzGR	Silver	35994	10
669XOQlRzY	Silver	98731	6
563nOpuQhE	Silver	67523	2
631zwMrPAW	Silver	70978	8
597yIUSTYB	Silver	94168	2
002ynxENls	Silver	56359	13
319wLHaxpA	Silver	60619	3
980KnHzfrS	Silver	20454	5
381MiPpwTt	Silver	82507	5
486rzsMYby	Silver	71077	13
957UYufSzN	Silver	95674	20
573pPAovsb	Silver	51899	11
410dgyFNpn	Silver	26106	1
117PGcBSeH	Silver	95853	17
998akQArEI	Silver	40730	17
576gpfuxNO	Silver	11356	2
996OkCGDBA	Silver	50148	5
853rOiDAhB	Silver	51635	1
459ilEodpD	Silver	71842	3
587Ensivxp	Silver	29112	11
042MlAbowZ	Silver	98226	4
641CMkRhny	Silver	56385	20
873XeWonSi	Silver	80354	14
771eXHsMtT	Silver	76797	19
791ySsAIOn	Silver	29691	6
031cEpsVwA	Silver	64495	3
051qZPRamN	Silver	34105	7
473OYljvzM	Silver	14883	15
170UyOCRmx	Silver	77930	13
523LFpiRPU	Silver	47190	13
745jsLMVBZ	Silver	15209	1
378TDOxqHw	Silver	93256	4
790foFmAdH	Silver	37899	12
703zMkAHZF	Silver	90877	18
006pvFlcnb	Silver	56975	11
767JqzdrDE	Silver	70674	11
497olytRzk	Silver	44648	15
961tiYnGxD	Silver	38021	12
913kwHRxzK	Silver	77793	13
603DwSpnyF	Silver	36098	7
015XczoKAy	Silver	20037	20
965CfOpIKt	Silver	17531	8
954XCdRkUB	Silver	22767	4
164KgxQmWe	Silver	97974	14
159LQCdtDg	Silver	36828	11
591WdwYSNM	Silver	19129	2
247GszTkqv	Silver	69503	14
476qpOVNbQ	Silver	31901	8
533CwpzDcn	Silver	37031	2
162zhNGyUQ	Silver	40987	19
943ZQYlvFB	Silver	64781	3
851ZlQqsKB	Silver	89419	2
530ZYTDplq	Silver	25958	11
933UCaJpBM	Silver	86669	4
068mHhZkxE	Silver	88950	5
748TpJvbnk	Silver	45461	1
747gwOGnUX	Silver	53249	16
045Cbeswxq	Silver	37922	20
967EVfvcKy	Silver	95620	10
548rxhygjQ	Silver	42651	14
468sVAhOpW	Silver	45065	1
621GScFZKL	Silver	88286	15
236EAkLbDm	Silver	24372	2
531NftuHay	Silver	75310	5
071qAWNmBv	Silver	78735	18
932hQZBCKP	Silver	92624	2
877dHkyXxu	Silver	12672	6
605amnjwSI	Silver	37257	7
870NokMalA	Silver	91655	6
354OZlwDpU	Silver	53373	3
339aXSZGbu	Silver	60639	20
540PVGskHT	Silver	72353	4
234oPZXKYA	Silver	59468	9
887GAXaCLU	Silver	52759	12
376fCoElRQ	Silver	41222	19
113fmbPTkB	Silver	47760	5
932VDZpgnr	Bronze	65239	10
607rKyDwjL	Bronze	75619	4
642CaOYGQz	Bronze	65689	8
189YPWiJqf	Bronze	24265	3
364HcZDUxl	Bronze	76816	9
593EFynrSv	Bronze	89220	1
489SLntDIO	Bronze	39957	13
269uIJzqOE	Bronze	35474	3
334ghumliD	Bronze	17724	10
796yvHCJuc	Bronze	83613	14
989sbAInxW	Bronze	11209	20
669hvoEBYF	Bronze	26035	2
603tnsafeB	Bronze	96594	5
700PJaNdLC	Bronze	50799	17
519akCvcKT	Bronze	50312	20
771FSxYJzB	Bronze	80459	18
240iOUyYHo	Bronze	28470	11
866Xrynjmp	Bronze	41383	10
019uSgpOWD	Bronze	41855	2
011TYnmbcG	Bronze	16262	7
983mXwuMlh	Bronze	12120	20
411SYiLmZN	Bronze	69460	18
738LzeQtqi	Bronze	79381	15
005pjEdctb	Bronze	33280	10
261QLCUzGR	Bronze	40961	16
669XOQlRzY	Bronze	91122	5
563nOpuQhE	Bronze	58414	20
631zwMrPAW	Bronze	92913	18
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.item (nama, deskripsi, usia_dari, usia_sampai, bahan) FROM stdin;
Electric Baby Swinger	Integer a nibh. In quis justo.	3	7	besi
Nerf	Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.	3	9	besi
Mahjong	Aenean fermentum. Donec ut mauris eget massa tempor convallis.	1	12	kayu
Rag Doll	In hac habitasse platea dictumst.	1	8	kapas
Lego	In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.	3	13	chemicals
Drawing Book	Vivamus tortor.	3	11	kapas
Baby Maracas	Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.	4	6	besi
Digital pet	Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.	5	15	besi
Robot Dog	Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.	4	8	plastik
Kaleidoskop	Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.	5	10	chemicals
Activity Walker	Ut at dolor quis odio consequat varius.	1	6	kapas
Water Gun	Praesent blandit lacinia erat.	4	5	kain
Hula Hoop	Phasellus sit amet erat.	2	6	besi
KNex	Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.	3	11	plastik
Jigsaw	Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst.	4	7	kain
Hot Wheels Car	Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.	1	9	kain
Chemistry Set	Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.	3	15	besi
Crayon	In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.	3	8	plastik
Teddy Bear	Vivamus vestibulum sagittis sapien.	3	13	kain
Jumping Jack	Suspendisse potenti.	5	6	chemicals
Megablocks	Quisque ut erat.	5	8	kain
Stroller Toy	Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.	5	7	kayu
Puzzle Lantai	Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.	1	15	kapas
B-Daman	Nulla ac enim.	1	13	besi
Mahkota Raja	Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.	5	10	kain
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.kategori (nama, level, sub_dari) FROM stdin;
Baby Walker	2	Aksesoris Bayi
Ayunan	2	Motorik Kasar
Aksesoris Bayi	1	\N
Motorik Halus	1	\N
Motorik Kasar	1	\N
\.


--
-- Data for Name: kategori_item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.kategori_item (nama_item, nama_kategori) FROM stdin;
Jumping Jack	Aksesoris Bayi
Chemistry Set	Ayunan
Stroller Toy	Motorik Halus
Digital pet	Aksesoris Bayi
Drawing Book	Motorik Kasar
Baby Maracas	Baby Walker
Kaleidoskop	Aksesoris Bayi
Lego	Motorik Kasar
Hula Hoop	Motorik Halus
Puzzle Lantai	Ayunan
Mahkota Raja	Motorik Kasar
Robot Dog	Motorik Kasar
Mahkota Raja	Baby Walker
Digital pet	Ayunan
Nerf	Motorik Kasar
Crayon	Motorik Kasar
Nerf	Motorik Halus
Nerf	Aksesoris Bayi
B-Daman	Ayunan
Hot Wheels Car	Aksesoris Bayi
Jigsaw	Baby Walker
Teddy Bear	Aksesoris Bayi
KNex	Aksesoris Bayi
Crayon	Motorik Halus
Rag Doll	Aksesoris Bayi
Puzzle Lantai	Motorik Kasar
Kaleidoskop	Motorik Kasar
Robot Dog	Aksesoris Bayi
Chemistry Set	Motorik Kasar
Mahkota Raja	Motorik Halus
Megablocks	Aksesoris Bayi
Drawing Book	Ayunan
Activity Walker	Ayunan
Megablocks	Motorik Kasar
Rag Doll	Motorik Kasar
Electric Baby Swinger	Motorik Halus
B-Daman	Baby Walker
Hot Wheels Car	Motorik Kasar
Robot Dog	Motorik Halus
Hula Hoop	Motorik Kasar
Puzzle Lantai	Motorik Halus
Puzzle Lantai	Baby Walker
Jigsaw	Aksesoris Bayi
Electric Baby Swinger	Aksesoris Bayi
Baby Maracas	Motorik Halus
Megablocks	Ayunan
Chemistry Set	Motorik Halus
Activity Walker	Baby Walker
Mahkota Raja	Ayunan
Lego	Ayunan
KNex	Motorik Halus
Jumping Jack	Motorik Kasar
Baby Maracas	Aksesoris Bayi
Rag Doll	Baby Walker
Drawing Book	Aksesoris Bayi
KNex	Motorik Kasar
Jigsaw	Motorik Halus
Jigsaw	Ayunan
Kaleidoskop	Motorik Halus
Jigsaw	Motorik Kasar
Electric Baby Swinger	Ayunan
Jumping Jack	Baby Walker
B-Daman	Motorik Kasar
Activity Walker	Motorik Halus
Mahkota Raja	Aksesoris Bayi
Baby Maracas	Motorik Kasar
Lego	Baby Walker
Mahjong	Motorik Kasar
Mahjong	Ayunan
Megablocks	Motorik Halus
Rag Doll	Motorik Halus
Water Gun	Motorik Kasar
Stroller Toy	Motorik Kasar
Water Gun	Aksesoris Bayi
Activity Walker	Aksesoris Bayi
Chemistry Set	Baby Walker
\.


--
-- Data for Name: level_keanggotaan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.level_keanggotaan (nama_level, minimum_poin, deskripsi) FROM stdin;
Silver	500	Awarded to those with minimum 500 poins
Bronze	100	Awarded to those with minimum 100 poins
Platinum	0	Level for those with less than 100 poins
Gold	800	Awarded to those with minimum 1000 poins
\.


--
-- Data for Name: pemesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.pemesanan (id_pemesanan, datetime_pesanan, kuantitas_barang, harga_sewa, ongkos, no_ktp_pemesan, status) FROM stdin;
1	2018-05-27 18:05:03	44	57748	23615	758864457	MENUNGGU_PEMBAYARAN
2	2018-07-31 14:06:08	1	68679	69498	205464713	DALAM_MASA_SEWA
3	2019-02-10 02:36:50	69	15743	56658	381265714	SEDANG_DIKONFIRMASI
4	2018-05-26 09:06:07	15	45068	95540	147162056	SUDAH_DIKEMBALIKAN
5	2018-10-23 23:14:26	40	43408	92977	864324233	MENUNGGU_PEMBAYARAN
6	2018-10-10 22:11:24	58	97872	19728	656374076	SUDAH_DIKEMBALIKAN
7	2018-11-23 22:50:04	29	30910	86010	631128376	SEDANG_DIKONFIRMASI
8	2018-09-29 01:54:56	40	37109	33422	115273830	SUDAH_DIKEMBALIKAN
9	2019-01-08 13:46:26	85	86525	10948	257070674	DALAM_MASA_SEWA
10	2018-12-25 22:01:33	18	60842	23312	70627880	SEDANG_DISIAPKAN
11	2018-08-31 07:47:35	1	90587	22785	721342626	SEDANG_DIKONFIRMASI
12	2018-04-15 01:39:38	36	31523	34022	456153051	SEDANG_DIKONFIRMASI
13	2019-03-24 02:38:33	16	66071	95227	635325146	SEDANG_DISIAPKAN
14	2018-08-14 06:33:41	84	44894	65329	635325146	MENUNGGU_PEMBAYARAN
15	2018-12-14 19:13:44	9	72786	76867	381265714	BATAL
16	2018-07-06 22:26:32	39	59494	21871	642371487	SEDANG_DIKONFIRMASI
17	2019-01-02 16:37:13	23	66283	26715	314631664	DALAM_MASA_SEWA
18	2019-02-13 00:16:21	69	14962	79769	26306611	SEDANG_DISIAPKAN
19	2019-04-05 16:20:46	88	22004	69827	687575673	MENUNGGU_PEMBAYARAN
20	2018-12-17 09:17:16	91	90052	39884	254150242	SUDAH_DIKEMBALIKAN
21	2018-10-03 12:08:12	82	15806	34125	115273830	DALAM_MASA_SEWA
22	2018-08-13 11:42:54	44	11227	94756	75314253	SUDAH_DIKEMBALIKAN
23	2018-04-29 01:36:33	30	63778	79975	531146331	SEDANG_DISIAPKAN
24	2018-10-20 00:24:55	80	80075	88714	526757528	SUDAH_DIKEMBALIKAN
25	2019-03-05 07:10:30	44	56713	20910	381265714	SUDAH_DIKEMBALIKAN
26	2018-07-10 00:28:16	30	41915	48539	741010433	MENUNGGU_PEMBAYARAN
27	2019-02-02 19:17:27	21	22692	32064	716231711	SUDAH_DIKEMBALIKAN
28	2018-11-08 03:08:39	59	34193	28262	268300780	SEDANG_DIKIRIM
29	2018-05-12 14:16:31	75	81829	60673	253553636	DALAM_MASA_SEWA
30	2018-11-17 19:22:57	62	81774	98695	658051512	SEDANG_DISIAPKAN
31	2019-02-07 10:14:10	94	98895	16976	47578854	SEDANG_DIKIRIM
32	2018-09-15 02:21:04	66	33743	35388	806070614	SUDAH_DIKEMBALIKAN
33	2018-11-15 19:15:18	39	81916	21696	488803118	DALAM_MASA_SEWA
34	2018-11-28 22:47:59	15	13965	21353	268076132	SEDANG_DIKONFIRMASI
35	2018-06-04 05:35:59	43	34165	93296	456153051	DALAM_MASA_SEWA
36	2018-10-18 19:30:30	39	26137	40421	642371487	SEDANG_DISIAPKAN
37	2018-12-09 06:06:24	60	50365	94930	105711383	SEDANG_DIKIRIM
38	2018-08-14 19:46:49	72	69910	62504	316421848	DALAM_MASA_SEWA
39	2018-06-26 19:02:32	46	67528	95914	381265714	SEDANG_DIKONFIRMASI
40	2018-11-04 20:51:08	6	26123	42035	205464713	SUDAH_DIKEMBALIKAN
41	2018-12-30 06:37:06	86	32852	74990	70627880	SEDANG_DIKONFIRMASI
42	2019-02-02 11:52:48	88	26051	63436	257070674	SUDAH_DIKEMBALIKAN
43	2019-03-30 02:22:12	93	47412	22715	552044235	DALAM_MASA_SEWA
44	2018-04-22 15:59:17	40	61244	82002	263281815	BATAL
45	2018-11-03 03:51:10	10	64401	28230	253553636	SUDAH_DIKEMBALIKAN
46	2018-11-26 09:44:42	35	78798	24187	130234473	DALAM_MASA_SEWA
47	2018-08-23 04:36:52	69	41890	38772	254150242	SUDAH_DIKEMBALIKAN
48	2018-04-23 04:09:28	85	20220	87825	156177876	SEDANG_DIKIRIM
49	2018-12-08 11:34:06	30	69317	73746	731720071	DALAM_MASA_SEWA
50	2018-09-01 00:13:33	57	78499	32717	65787275	SUDAH_DIKEMBALIKAN
\.


--
-- Data for Name: pengembalian; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.pengembalian (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
100001	1	same day	89115	2018-12-05	531146331	House
100002	2	next day	23969	2019-04-17	65787275	Office
100003	3	same day	43547	2018-09-19	260351280	Office
100004	4	next day	69148	2019-02-12	272253762	House
100005	5	same day	53292	2018-08-03	600737262	Apartment
100006	6	regular	27399	2018-12-12	656374076	House
100007	7	cargo	76040	2018-11-20	832805316	Apartment
100008	8	regular	97940	2018-06-11	537106202	Estate
100009	9	same day	66756	2019-03-24	223650412	House
100010	10	cargo	12503	2019-01-08	741010433	Office
100011	11	regular	15222	2019-02-02	527034322	Dorm
100012	12	cargo	72387	2018-07-10	251367872	Office
100013	13	next day	35894	2019-04-25	766878206	House
100014	14	regular	57886	2018-06-17	503723662	Apartment
100015	15	cargo	70940	2018-04-12	314631664	Apartment
100016	16	cargo	51755	2018-04-30	385158543	Dorm
100017	17	next day	22295	2018-04-13	102250071	Office
100018	18	same day	76550	2018-08-04	844441486	Office
100019	19	cargo	56335	2018-05-08	754822233	Dorm
100020	20	regular	10997	2018-07-02	47578854	House
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.pengguna (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) FROM stdin;
846603383	Erick Armstrong	Erick_Armstrong6691@guentu.biz	1994-07-19	22502400250
723132618	Colleen Edley	Colleen_Edley9860@yahoo.com	1977-08-18	47420256550
667334564	Chris Thomson	Chris_Thomson4990@tonsy.org	1979-08-03	3317774268
476641275	Ron Judd	Ron_Judd5306@famism.biz	1999-10-15	5080415488
707580576	Jenna Plumb	Jenna_Plumb9635@typill.biz	1993-03-23	11644218732
102287558	Percy Noon	Percy_Noon3649@elnee.tech	1983-11-04	10116271336
606335605	Noah Willson	Noah_Willson7417@irrepsy.com	1996-01-09	12877621336
143882080	Leilani Mooney	Leilani_Mooney5898@eirey.tech	1975-03-29	57226207782
181044377	Bob Haines	Bob_Haines9674@womeona.net	1975-03-07	53547653131
12657044	Carina Sawyer	Carina_Sawyer8014@yahoo.com	1980-02-17	73253156722
531146331	Carolyn Palmer	Carolyn_Palmer9425@fuliss.net	1974-03-20	42540388668
65787275	Benny Baxter	Benny_Baxter7177@brety.org	1975-12-24	55218305150
260351280	Aurelia Morgan	Aurelia_Morgan6003@vetan.org	1987-07-18	68405116503
272253762	David Asher	David_Asher6051@infotech44.tech	1972-02-29	47446364054
600737262	Chuck Abbot	Chuck_Abbot422@bauros.biz	1987-02-18	13640027043
656374076	Harry Richards	Harry_Richards1971@ovock.tech	1974-07-14	70665817468
832805316	Lindsay Cox	Lindsay_Cox5202@famism.biz	1971-07-25	46424135715
537106202	Denis Matthews	Denis_Matthews1657@qater.org	1995-01-20	63312803441
223650412	Thea Ebbs	Thea_Ebbs3032@infotech44.tech	1983-02-14	36333171224
741010433	Lexi Wright	Lexi_Wright3308@hourpy.biz	1984-06-09	10210077581
527034322	Denis Eastwood	Denis_Eastwood1250@grannar.com	1990-06-02	44370111674
251367872	Lana Robinson	Lana_Robinson2702@muall.tech	1983-07-21	50766047641
766878206	Hayden King	Hayden_King2518@eirey.tech	1971-06-15	11620165544
503723662	Stephanie Sawyer	Stephanie_Sawyer942@zorer.org	1978-03-20	40835423275
314631664	Ronald Warner	Ronald_Warner3446@gmail.com	1998-01-14	36378144284
385158543	Kaylee Norman	Kaylee_Norman108@nanoff.biz	1971-08-16	7006701130
102250071	Carina Foxley	Carina_Foxley8880@sheye.org	1976-10-27	57678213050
844441486	Clint Russell	Clint_Russell4422@typill.biz	1994-11-09	23847553027
754822233	Alan Mcnally	Alan_Mcnally4109@gompie.com	1988-10-18	4822711007
47578854	Benny Clifford	Benny_Clifford875@irrepsy.com	1993-12-26	82471078756
721342626	Johnathan Lewin	Johnathan_Lewin1839@atink.com	1979-04-21	64785073084
683126206	Anthony Mccormick	Anthony_Mccormick2391@infotech44.tech	1998-07-09	63440108321
43218620	Susan Mcguire	Susan_Mcguire1715@jiman.org	1996-02-05	87610348557
283155418	Stephanie Salt	Stephanie_Salt5663@acrit.org	1975-06-10	28213343280
353707750	Martin Weasley	Martin_Weasley3033@typill.biz	1984-07-08	76415364185
635325146	Bryon Moreno	Bryon_Moreno97@brety.org	1988-08-21	36141325662
278252278	Chris Mccall	Chris_Mccall202@cispeto.com	1972-11-18	33614713448
758864457	Barney Walsh	Barney_Walsh3619@nickia.com	1987-07-03	86880234460
716231711	Raquel Moore	Raquel_Moore197@iatim.tech	1992-07-12	43852470362
326388820	Maia Collins	Maia_Collins9833@irrepsy.com	1971-08-24	33801021155
203456425	Danny Osman	Danny_Osman3634@sveldo.biz	1978-03-06	1200023412
273327253	Holly Wright	Holly_Wright7152@joiniaa.com	1990-04-28	87671032718
115273830	Melanie Weasley	Melanie_Weasley296@zorer.org	1978-09-12	67366524416
35242664	Joy Lunt	Joy_Lunt7594@twipet.com	1975-09-09	5735135344
631128376	Jacob Walsh	Jacob_Walsh5038@liret.org	1971-10-25	31487448532
607146165	Ronald Spencer	Ronald_Spencer6468@womeona.net	1997-10-02	14048830287
254150242	Enoch Warner	Enoch_Warner9921@deons.tech	1980-07-09	37815481660
117303313	Tom Tutton	Tom_Tutton3309@qater.org	1971-10-02	48571807451
82101555	Estrella Cork	Estrella_Cork443@twipet.com	1999-12-07	35071274582
456153051	Morgan Calderwood	Morgan_Calderwood7803@grannar.com	1988-02-08	56312101675
642371487	Cadence Powell	Cadence_Powell4203@bretoux.com	1973-05-16	27758817513
75314253	Amy Torres	Amy_Torres9521@liret.org	1987-11-27	77527407764
418037730	Cherish Willis	Cherish_Willis2183@nanoff.biz	1980-12-26	32037727312
526757528	Liam Dempsey	Liam_Dempsey516@joiniaa.com	1986-01-10	48434562
205464713	Tiffany Saunders	Tiffany_Saunders8496@liret.org	1982-12-12	87143212157
381265714	Carla King	Carla_King8162@muall.tech	1997-10-30	28084581176
257070674	Keira Flett	Keira_Flett6040@jiman.org	1978-08-17	73610856482
147162056	Alexander Walker	Alexander_Walker7943@tonsy.org	1977-09-19	58257338216
231353044	Benny Parr	Benny_Parr2826@nimogy.biz	1986-01-18	83283788631
130234473	Logan Plumb	Logan_Plumb5846@tonsy.org	1995-01-09	71370364284
488803118	Johnny Lewis	Johnny_Lewis4570@deavo.com	1994-01-04	63628716510
705058635	Cara Higgs	Cara_Higgs8916@grannar.com	1980-07-04	13857546840
241171244	Savannah Jackson	Savannah_Jackson6991@sheye.org	1978-08-09	67206360784
253553636	Mavis Jarrett	Mavis_Jarrett2607@gompie.com	1973-12-04	77067267540
181741421	Gwenyth Paterson	Gwenyth_Paterson9026@elnee.tech	1982-08-01	42286814243
687575673	Deborah Avery	Deborah_Avery6322@irrepsy.com	1996-09-25	26377127530
316053172	Ryan Speed	Ryan_Speed1439@dionrab.com	1997-07-27	45303070777
721670765	Juliet Oakley	Juliet_Oakley4255@bretoux.com	1985-01-06	62531076801
864324233	Maxwell Oswald	Maxwell_Oswald6211@tonsy.org	1995-08-27	65267151062
88665010	Gabriel Hale	Gabriel_Hale4392@deavo.com	1982-11-14	20306147215
217002248	Chelsea Connell	Chelsea_Connell5355@mafthy.com	1973-07-29	80571757227
744427733	Marla Moreno	Marla_Moreno3244@tonsy.org	1993-06-27	37418200102
268300780	Bryon Terry	Bryon_Terry8805@qater.org	1971-09-21	50007833382
316421848	Ethan Hardwick	Ethan_Hardwick9573@gompie.com	1998-07-23	24847402037
61546133	Trisha Shepherd	Trisha_Shepherd3599@yahoo.com	1986-12-25	40757043846
116187483	William Ross	William_Ross9050@ubusive.com	1991-09-24	44002530826
251471352	Dasha Farrell	Dasha_Farrell9877@dionrab.com	1973-11-25	25236406485
105711383	Aiden Cooper	Aiden_Cooper2906@kideod.biz	1995-08-15	4030553341
731720071	Liam West	Liam_West425@bungar.biz	1990-11-27	83085434803
375275174	Elijah Reynolds	Elijah_Reynolds3086@tonsy.org	1981-10-19	45703300118
220676076	Tyler Sawyer	Tyler_Sawyer6869@typill.biz	1988-07-01	77234445803
263281815	Dani Duvall	Dani_Duvall1443@sveldo.biz	1974-02-16	27682225377
86524244	Rocco Bright	Rocco_Bright7893@infotech44.tech	1970-07-22	21074516503
552044235	Kurt Harvey	Kurt_Harvey1373@sveldo.biz	1977-11-04	81753067752
764747116	Tyson Cowan	Tyson_Cowan1565@joiniaa.com	1993-07-16	67267575414
268076132	Caydence Allwood	Caydence_Allwood1998@liret.org	1993-11-04	17538170600
285653661	Tiffany Dempsey	Tiffany_Dempsey4005@bulaffy.com	1987-12-21	45138338035
806070614	Ryan Tanner	Ryan_Tanner8589@typill.biz	1994-05-28	42170248125
658051512	Savannah Olson	Savannah_Olson2753@hourpy.biz	1977-12-29	53727514287
156177876	Jocelyn Becker	Jocelyn_Becker9400@zorer.org	1987-03-10	84275586880
522780470	Doug Kennedy	Doug_Kennedy9698@hourpy.biz	1974-02-08	48135244358
477801811	Emma Riley	Emma_Riley9307@hourpy.biz	1972-04-11	81277487380
244405078	Morgan Hunt	Morgan_Hunt4495@bulaffy.com	1997-08-03	58261436853
152737323	Mike Carpenter	Mike_Carpenter9393@qater.org	1977-03-05	80865706000
608687105	George Mann	George_Mann1303@infotech44.tech	1994-07-15	77358853146
26306611	Leslie Todd	Leslie_Todd9508@bauros.biz	1997-02-09	77024116471
373374216	Hayden Coleman	Hayden_Coleman1140@muall.tech	1998-08-09	76535356851
657567004	Candace Slater	Candace_Slater1452@fuliss.net	1992-10-06	53162614873
687150224	Jackeline Kelly	Jackeline_Kelly8965@supunk.biz	1983-10-12	34477328737
70627880	Lana Rowlands	Lana_Rowlands6234@womeona.net	1986-01-15	34720428875
\.


--
-- Data for Name: pengiriman; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.pengiriman (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
100001	10	regular	52577	2018-10-08	531146331	House
100002	12	same day	42693	2018-11-15	65787275	Office
100003	24	next day	27643	2018-08-12	260351280	Office
100004	17	same day	64594	2019-03-13	272253762	House
100005	35	cargo	46748	2018-12-20	600737262	Apartment
100006	35	same day	27858	2018-08-27	656374076	House
100007	35	cargo	78424	2018-10-21	832805316	Apartment
100008	34	next day	22729	2019-03-09	537106202	Estate
100009	23	regular	19895	2018-06-07	223650412	House
100010	9	next day	33008	2018-09-29	741010433	Office
100011	21	next day	85483	2018-07-15	527034322	Dorm
100012	49	same day	18598	2019-03-12	251367872	Office
100013	45	same day	41155	2018-10-21	766878206	House
100014	37	same day	98253	2018-09-04	503723662	Apartment
100015	34	cargo	97168	2019-01-07	314631664	Apartment
100016	50	cargo	82025	2018-04-25	385158543	Dorm
100017	41	cargo	93038	2018-06-05	102250071	Office
100018	12	cargo	89266	2018-10-14	844441486	Office
100019	13	next day	45051	2018-12-14	754822233	Dorm
100020	2	next day	30521	2018-11-29	47578854	House
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: toys_rent; Owner: db2018025
--

COPY toys_rent.status (nama, deskripsi) FROM stdin;
SEDANG_DIKONFIRMASI	sedang dalam masa konfirmasi oleh admin peminjaman
MENUNGGU_PEMBAYARAN	menunggu pembayaran dari peminjam
SEDANG_DISIAPKAN	barang sedang disiapkan oleh logistik
SEDANG_DIKIRIM	barang sedang dikirim oleh kurir
DALAM_MASA_SEWA	sedang dalam masa sewa
SUDAH_DIKEMBALIKAN	barang sudah dikembalikan
BATAL	pemesanan dibatalkan
\.


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (no_ktp);


--
-- Name: alamat alamat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.alamat
    ADD CONSTRAINT alamat_pkey PRIMARY KEY (no_ktp_anggota, nama);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_ktp);


--
-- Name: barang_dikembalikan barang_dikembalikan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_dikirim barang_dikirim_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_pesanan barang_pesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_pkey PRIMARY KEY (id_pemesanan, no_urut);


--
-- Name: barang barang_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_pkey PRIMARY KEY (id_barang);


--
-- Name: chat chat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);


--
-- Name: info_barang_level info_barang_level_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_pkey PRIMARY KEY (id_barang, nama_level);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (nama);


--
-- Name: kategori_item kategori_item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_pkey PRIMARY KEY (nama_item, nama_kategori);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (nama);


--
-- Name: level_keanggotaan level_keanggotaan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.level_keanggotaan
    ADD CONSTRAINT level_keanggotaan_pkey PRIMARY KEY (nama_level);


--
-- Name: pemesanan pemesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_pkey PRIMARY KEY (id_pemesanan);


--
-- Name: pengembalian pengembalian_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_pkey PRIMARY KEY (no_resi);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (no_ktp);


--
-- Name: pengiriman pengiriman_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_pkey PRIMARY KEY (no_resi);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (nama);


--
-- Name: barang_pesanan trigger_update_harga_sewa; Type: TRIGGER; Schema: toys_rent; Owner: db2018025
--

CREATE TRIGGER trigger_update_harga_sewa AFTER INSERT OR DELETE OR UPDATE ON toys_rent.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_harga_sewa();


--
-- Name: pengiriman trigger_update_ongkos; Type: TRIGGER; Schema: toys_rent; Owner: db2018025
--

CREATE TRIGGER trigger_update_ongkos AFTER INSERT OR DELETE OR UPDATE ON toys_rent.pengiriman FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_ongkos();


--
-- Name: barang_pesanan update_kondisi_barang; Type: TRIGGER; Schema: toys_rent; Owner: db2018025
--

CREATE TRIGGER update_kondisi_barang AFTER INSERT OR DELETE OR UPDATE ON toys_rent.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_kondisi_barang();


--
-- Name: level_keanggotaan update_level_keanggotaan; Type: TRIGGER; Schema: toys_rent; Owner: db2018025
--

CREATE TRIGGER update_level_keanggotaan AFTER INSERT OR DELETE OR UPDATE ON toys_rent.level_keanggotaan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_level_keanggotaan();


--
-- Name: anggota update_level_keanggotaan_anggota; Type: TRIGGER; Schema: toys_rent; Owner: db2018025
--

CREATE TRIGGER update_level_keanggotaan_anggota AFTER INSERT OR UPDATE OF poin ON toys_rent.anggota FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_level_keanggotaan_anggota();


--
-- Name: admin admin_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.admin
    ADD CONSTRAINT admin_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toys_rent.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: alamat alamat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.alamat
    ADD CONSTRAINT alamat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toys_rent.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_level_fkey FOREIGN KEY (level) REFERENCES toys_rent.level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toys_rent.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan barang_dikembalikan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan barang_dikembalikan_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengembalian(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikirim barang_dikirim_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikirim barang_dikirim_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengiriman(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang barang_nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toys_rent.item(nama);


--
-- Name: barang barang_no_ktp_penyewa_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_no_ktp_penyewa_fkey FOREIGN KEY (no_ktp_penyewa) REFERENCES toys_rent.anggota(no_ktp);


--
-- Name: barang_pesanan barang_pesanan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_status_fkey FOREIGN KEY (status) REFERENCES toys_rent.status(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat chat_no_ktp_admin_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_no_ktp_admin_fkey FOREIGN KEY (no_ktp_admin) REFERENCES toys_rent.admin(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat chat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toys_rent.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori fk_sub_dari; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.kategori
    ADD CONSTRAINT fk_sub_dari FOREIGN KEY (sub_dari) REFERENCES toys_rent.kategori(nama);


--
-- Name: info_barang_level info_barang_level_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: info_barang_level info_barang_level_nama_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_nama_level_fkey FOREIGN KEY (nama_level) REFERENCES toys_rent.level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_item nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toys_rent.item(nama);


--
-- Name: kategori_item nama_kategori_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT nama_kategori_fkey FOREIGN KEY (nama_kategori) REFERENCES toys_rent.kategori(nama);


--
-- Name: pemesanan pemesanan_no_ktp_pemesan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_no_ktp_pemesan_fkey FOREIGN KEY (no_ktp_pemesan) REFERENCES toys_rent.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pemesanan pemesanan_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_status_fkey FOREIGN KEY (status) REFERENCES toys_rent.status(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toys_rent.alamat(no_ktp_anggota, nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengiriman(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengiriman pengiriman_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengiriman pengiriman_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018025
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toys_rent.alamat(no_ktp_anggota, nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

-- REVOKE ALL ON SCHEMA public FROM PUBLIC;
-- REVOKE ALL ON SCHEMA public FROM postgres;
-- GRANT ALL ON SCHEMA public TO postgres;
-- GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

