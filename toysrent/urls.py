"""toysrent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import include, url

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('landing.urls')),
    url(r'^pengguna/', include(('pengguna.urls', 'pengguna'), namespace='pengguna')),
    path('barang/', include('barang.urls')),
    path('pengiriman/', include('pengiriman.urls')),
    path('review/', include('review.urls')),
    path('chat/', include('chat.urls')),
    url(r'^item/', include(('item.urls', 'item'), namespace='item')),
    path('level/', include('level.urls')),
    path('pesanan/', include('pesanan.urls')),
    url(r'pengguna/', include(('pengguna.urls', 'pengguna'), namespace='pengguna')),
]
