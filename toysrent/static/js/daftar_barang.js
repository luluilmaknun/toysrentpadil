var countCheck = 0;

$(document).ready(function () {
    $(document).on('click', '.checkPesan', function(){
        if($(this).prop("checked")){
            if(countCheck == 0) $('#pesan').removeClass('sr-only');
            countCheck += 1;
        }
        else{
            countCheck -= 1;
            if(countCheck == 0) $('#pesan').addClass('sr-only');
        }
    });
});