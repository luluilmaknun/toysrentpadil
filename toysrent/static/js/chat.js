var messageToSend = '';

function cacheDOM() {
    this.$chatHistory = $('.chat-history');
    this.$button = $('button');
    this.$textarea = $('#message-to-send');
    this.$chatHistoryList =  this.$chatHistory.find('ul');
}

function bindEvents() {
    this.$button.on('click', this.addMessage.bind(this));
    this.$textarea.on('keyup', this.addMessageEnter.bind(this));
}

function render() {
    this.scrollToBottom();
    if (this.messageToSend.trim() !== '') {
        var context = '<li class="clearfix">'
            + '<div class="message-data align-right">'
            + '<span class="message-data-name" >You</span>'
            + '</div>'
            + '<div class="message other-message float-right">'
            + this.messageToSend
            + '</div>'
            + '</li>';
        // var template = Handlebars.compile( $("#message-template").html());
        // var context = { 
        //     messageOutput: this.messageToSend,
        // };

        // this.$chatHistoryList.append(template(context));
        this.$chatHistoryList.append(context);
        this.scrollToBottom();
        this.$textarea.val('');

        // responses
        // var templateResponse = Handlebars.compile( $("#message-response-template").html());
        // var contextResponse = { 
        //   response: 'test',
        // };

        // setTimeout(function() {
        //     this.$chatHistoryList.append(templateResponse(contextResponse));
        //     this.scrollToBottom();
        // }.bind(this), 1500);
    }
}

function init() {
    this.cacheDOM();
    this.bindEvents();
    this.render();
}

function addMessage() {
    this.messageToSend = this.$textarea.val()
    this.render();    
}

function addMessageEnter() {
    // enter was pressed
    if (event.keyCode === 13) {
        this.addMessage();
    }
}

function scrollToBottom() {
    this.$chatHistory.scrollTop(this.$chatHistory[0].scrollHeight);
}

$(document).ready(function () {
    init();
});