$(document).ready(function() {
	$('.Add-Alamat').click(function() {
		$('.alamat-row:first').clone().removeClass("d-none").appendTo('.alamats');
	});

	$(".alamats").delegate(".Delete-Alamat", "click", function() {
		if ($('.alamat-row').length > 2) {
			$(this).parent().remove();
		}
	});
});