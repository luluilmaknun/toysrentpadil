$(document).ready(function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
                }
            }
        }
        return cookieValue;
    }
    
    var csrftoken = getCookie('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $(document).on('submit', '#form-registration',function(e) {
        if(!$('#submit').hasClass('disabled')) {
            var noItem = document.getElementById("pemilik");
            var noPemilik = document.getElementById("namaItem");

            e.preventDefault();
            $.ajax({
                type:'POST',
                url:'/barang/update_barang/',
                data:{
                    idBarang:$('#idBarang').val(),
                    namaItem:noItem.options[noItem.selectedIndex].text,
                    warna:$('#warna').val(),
                    urlFoto:$('#urlFoto').val(),
                    kondisi:$('#kondisi').val(),
                    lamaPenggunaan:$('#lamaPenggunaan').val(),
                    pemilik:noPemilik.options[noPemilik.selectedIndex].text,
                    persenRoyaltyBronze:$('#persenRoyaltyBronze').val(),
                    hargaSewaBronze:$('#hargaSewaBronze').val(),
                    persenRoyaltySilver:$('#persenRoyaltySilver').val(),
                    hargaSewaSilver:$('#hargaSewaSilver').val(),
                    persenRoyaltyGold:$('#persenRoyaltyGold').val(),
                    hargaSewaGold:$('#hargaSewaGold').val(),
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                },
                sucess:function() {
                }
            });
        } else {
            return false;
        }
    });
});