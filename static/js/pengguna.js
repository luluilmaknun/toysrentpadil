var alamat_counter = 1;

$(document).ready(function() {
	$('.Add-Alamat').click(function() {
		var new_alamat = $('#alamat-row').clone().removeClass("d-none");
		var inputs = new_alamat.find('input');
		alamat_counter += 1;
		$.each(inputs, function(index, elem){
            var element = $(elem);
            var name = element.prop('name');
            name += alamat_counter;
            element.prop('name', name);
        });
		new_alamat.attr("id", "alamat-row-" + alamat_counter);
		new_alamat.appendTo('.alamats');
	});

	$(".alamats").delegate(".Delete-Alamat", "click", function() {
		if ($('.alamat-row').length > 2) {
			$(this).parent().remove();
			alamat_counter -= 1;
		}
	});
});