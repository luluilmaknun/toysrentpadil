from django.urls import path
from .views import *
#url for app

app_name = 'barang'
urlpatterns = [
    path('', daftar_barang, name='daftar_barang'),
	path('tambah_barang', tambah_barang, name='tambah_barang'),
    path('<kunci>/detail_barang', detail_barang, name='detail_barang'),
	path('<kunci>/update_barang', update_barang, name='update_barang'),
	path('<kunci>/delete_barang', delete_barang, name='delete_barang')
]