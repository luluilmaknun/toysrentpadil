from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect

# Create your views here.
def daftar_barang(request):
    response = {}

    with connection.cursor() as cursor1:
        cursor1.execute("SELECT * FROM BARANG")
        response["daftar_barang"] = cursor1.fetchall()

    with connection.cursor() as cursor2:
        cursor2.execute("SELECT DISTINCT nama_item FROM BARANG")
        response["filters"] = cursor2.fetchall()
    
    return render(request, 'daftar_barang.html', response)

def tambah_barang(request):
    response = {}

    with connection.cursor() as cursor1:
        cursor1.execute("SELECT DISTINCT nama_item FROM BARANG")
        response["items"] = cursor1.fetchall()

    with connection.cursor() as cursor2:
        cursor2.execute("SELECT DISTINCT P.nama_lengkap FROM PENGGUNA P, BARANG B WHERE B.no_ktp_penyewa = P.no_ktp")
        response["owners"] = cursor2.fetchall()

    if (request.method == "POST"):
        idBarang = request.POST['idBarang']
        namaItem = request.POST['namaItem']
        warna = request.POST['warna']
        urlFoto = request.POST['urlFoto']
        kondisi = request.POST['kondisi']
        lamaPenggunaan = request.POST['lamaPenggunaan']
        pemilik = request.POST['pemilik']
        persenRoyaltyBronze = request.POST['persenRoyaltyBronze']
        hargaSewaBronze = request.POST['hargaSewaBronze']
        persenRoyaltySilver = request.POST['persenRoyaltySilver']
        hargaSewaSilver = request.POST['hargaSewaSilver']
        persenRoyaltyGold = request.POST['persenRoyaltyGold']
        hargaSewaGold = request.POST['hargaSewaGold']

        noKTP = ""
        with connection.cursor() as cursor3:
            cursor3.execute("SELECT no_ktp FROM PENGGUNA WHERE nama_lengap = pemilik")
            noKTP = cursor3.fetchone()[0]


        with connection.cursor() as cursor4:
            cursor4.execute("INSERT INTO BARANG VALUES(%s, %s, %s, %s, %s, %s, %s)", [idBarang, namaItem, warna, urlFoto, kondisi, lamaPenggunaan, noKTP])
            cursor4.execute("INSERT INTO INFO_BARANG_LEVEL VALUES(%s, 'Bronze', %s, %s)", [idBarang, hargaSewaBronze, persenRoyaltyBronze])
            cursor4.execute("INSERT INTO INFO_BARANG_LEVEL VALUES(%s, 'Silver', %s, %s)", [idBarang, hargaSewaSilver, persenRoyaltySilver])
            cursor4.execute("INSERT INTO INFO_BARANG_LEVEL VALUES(%s, 'Gold', %s, %s)", [idBarang, hargaSewaGold, persenRoyaltyGold])

        return redirect("/barang/")

    return render(request, 'tambah_barang.html', response)

def update_barang(request, kunci):
    response = {}

    with connection.cursor() as cursor1:
        cursor1.execute("SELECT DISTINCT nama_item FROM BARANG")
        response["items"] = cursor1.fetchall()

    with connection.cursor() as cursor2:
        cursor2.execute("SELECT DISTINCT P.nama_lengkap FROM PENGGUNA P, BARANG B WHERE B.no_ktp_penyewa = P.no_ktp")
        response["owners"] = cursor2.fetchall()

    if (request.method == "POST"):
        idBarang = request.POST['idBarang']
        namaItem = request.POST['namaItem']
        warna = request.POST['warna']
        urlFoto = request.POST['urlFoto']
        kondisi = request.POST['kondisi']
        lamaPenggunaan = request.POST['lamaPenggunaan']
        pemilik = request.POST['pemilik']
        persenRoyaltyBronze = request.POST['persenRoyaltyBronze']
        hargaSewaBronze = request.POST['hargaSewaBronze']
        persenRoyaltySilver = request.POST['persenRoyaltySilver']
        hargaSewaSilver = request.POST['hargaSewaSilver']
        persenRoyaltyGold = request.POST['persenRoyaltyGold']
        hargaSewaGold = request.POST['hargaSewaGold']

        noKTP = ""
        with connection.cursor() as cursor3:
            cursor3.execute("SELECT no_ktp FROM PENGGUNA WHERE nama_lengap = pemilik")
            noKTP = cursor3.fetchone()[0]

        with connection.cursor() as cursor4:
            cursor4.execute("UPDATE BARANG SET nama_item = %s, warna = %s, url_foto = %s, kondisi = %s, lama_penggunaan = %s, no_ktp_penyewa = %s  WHERE id_barang = %s", [namaItem, warna, urlFoto, kondisi, lamaPenggunaan, noKTP, idBarang])
            cursor4.execute("UPDATE INFO_BARANG_LEVEL SET harga_sewa = %s, porsi_loyalti = %s  WHERE id_barang = %s AND nama_level = 'Bronze'", [hargaSewaBronze, persenRoyaltyBronze, idBarang])
            cursor4.execute("UPDATE INFO_BARANG_LEVEL SET harga_sewa = %s, porsi_loyalti = %s  WHERE id_barang = %s AND nama_level = 'Silver'", [hargaSewaSilver, persenRoyaltySilver, idBarang])
            cursor4.execute("UPDATE INFO_BARANG_LEVEL SET harga_sewa = %s, porsi_loyalti = %s  WHERE id_barang = %s AND nama_level = 'Gold'", [hargaSewaGold, persenRoyaltyGold, idBarang])
        return redirect("/barang/")

    data_barang = []
    with connection.cursor() as cursor5:
        cursor5.execute("SELECT * FROM BARANG WHERE id_barang = %s", [kunci])
        data_barang = cursor5.fetchone()

    data_bronze = []
    with connection.cursor() as cursor6:
        cursor6.execute("SELECT * FROM INFO_BARANG_LEVEL WHERE id_barang = %s AND nama_level = 'Bronze'", [kunci])
        data_bronze = cursor6.fetchone()
    
    data_silver = []
    with connection.cursor() as cursor7:
        cursor7.execute("SELECT * FROM INFO_BARANG_LEVEL WHERE id_barang = %s AND nama_level = 'Silver'", [kunci])
        data_silver = cursor7.fetchone()

    data_gold = []
    with connection.cursor() as cursor8:
        cursor8.execute("SELECT * FROM INFO_BARANG_LEVEL WHERE id_barang = %s AND nama_level = 'Gold'", [kunci])
        data_gold = cursor8.fetchone()

    response["id_barang"] = data_barang[0]
    response["nama_item"] = data_barang[1]
    response["warna"] = data_barang[2]
    response["url_foto"] = data_barang[3]
    response["kondisi"] = data_barang[4]
    response["lama_penggunaan"] = data_barang[5]

    with connection.cursor() as cursor9:
        cursor9.execute("SELECT nama_lengkap FROM PENGGUNA WHERE no_ktp = %s", [data_barang[6]])
        response["nama_pemilik"] = cursor9.fetchone()[0]

    response["harga_sewa_bronze"] = data_bronze[2]
    response["porsi_loyalti_bronze"] = data_bronze[3]
    response["harga_sewa_silver"] = data_silver[2]
    response["porsi_loyalti_silver"] = data_silver[3]
    response["harga_sewa_gold"] = data_gold[2]
    response["porsi_loyalti_gold"] = data_gold[3]

    return render(request, 'update_barang.html', response)

def delete_barang(request, kunci):
	with connection.cursor() as cursor:
		cursor.execute("DELETE FROM BARANG WHERE id_barang = %s", [kunci])
	return redirect("/barang/")

def detail_barang(request, kunci):
    response = {}

    with connection.cursor() as cursor1:
        cursor1.execute("SELECT P.nama_lengkap, BD.review FROM BARANG_DIKIRIM BD, PENGIRIMAN PN , PENGGUNA P WHERE BD.id_barang = %s AND BD.no_resi = PN.no_resi AND PN.no_ktp_anggota = P.no_ktp", [kunci])
        response["daftar_review"] = cursor1.fetchall()

    data_barang = []
    with connection.cursor() as cursor5:
        cursor5.execute("SELECT * FROM BARANG WHERE id_barang = %s", [kunci])
        data_barang = cursor5.fetchone()

    data_bronze = []
    with connection.cursor() as cursor6:
        cursor6.execute("SELECT * FROM INFO_BARANG_LEVEL WHERE id_barang = %s AND nama_level = 'Bronze'", [kunci])
        data_bronze = cursor6.fetchone()
    
    data_silver = []
    with connection.cursor() as cursor7:
        cursor7.execute("SELECT * FROM INFO_BARANG_LEVEL WHERE id_barang = %s AND nama_level = 'Silver'", [kunci])
        data_silver = cursor7.fetchone()

    data_gold = []
    with connection.cursor() as cursor8:
        cursor8.execute("SELECT * FROM INFO_BARANG_LEVEL WHERE id_barang = %s AND nama_level = 'Gold'", [kunci])
        data_gold = cursor8.fetchone()

    response["id_barang"] = data_barang[0]
    response["nama_item"] = data_barang[1]
    response["warna"] = data_barang[2]
    response["url_foto"] = data_barang[3]
    response["kondisi"] = data_barang[4]
    response["lama_penggunaan"] = data_barang[5]

    with connection.cursor() as cursor9:
        cursor9.execute("SELECT nama_lengkap FROM PENGGUNA WHERE no_ktp = %s", [data_barang[6]])
        response["nama_pemilik"] = cursor9.fetchone()[0]

    response["harga_sewa_bronze"] = data_bronze[2]
    response["porsi_loyalti_bronze"] = data_bronze[3]
    response["harga_sewa_silver"] = data_silver[2]
    response["porsi_loyalti_silver"] = data_silver[3]
    response["harga_sewa_gold"] = data_gold[2]
    response["porsi_loyalti_gold"] = data_gold[3]

    return render(request, 'detail_barang.html', response)

