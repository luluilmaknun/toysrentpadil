from django.shortcuts import render
from .forms import *
from django.db import connection
from django.shortcuts import redirect

def index(request):
	response = {}
	response['items'] = []
	with connection.cursor() as cursor:
		cursor.execute("SELECT NAMA FROM ITEM")
		items = cursor.fetchall()

		for i in range(len(items)):
			cursor.execute("SELECT NAMA_KATEGORI FROM KATEGORI_ITEM WHERE NAMA_ITEM = %s", [items[i]])
			categories_tuple = cursor.fetchall()

			categories = []
			for category in categories_tuple:
				categories.append(category[0])
			response['items'].append([items[i][0], ', '.join(categories)])

		return render(request, "index.html", response)

def tambah_item(request):
	form = item(request.POST or None)
	if (request.method == "POST" and form.is_valid()):
			nama = request.POST['nama']
			deskripsi = request.POST['deskripsi']
			usia_dari = request.POST['usia_dari']
			usia_sampai = request.POST['usia_sampai']
			bahan = request.POST['bahan']
			categories = request.POST.getlist('category')
			if (check_availability(nama)):
				with connection.cursor() as cursor:
					cursor.execute("INSERT INTO ITEM VALUES(%s, %s, %s, %s, %s)", [nama, deskripsi, usia_dari, usia_sampai, bahan])
					for category in categories:
						cursor.execute("INSERT INTO KATEGORI_ITEM VALUES(%s, %s)", [nama, category])
				return redirect("/item")
			else:
				form.add_error("deskripsi", "Tidak bisa mendaftarkan item dengan nama tersebut!")
				response = {}
				response['form'] = form
				response['type'] = 'tambah'
				response['categories'] = semua_nama_kategori()
				return render(request, 'cu-item.html', response)

def tambah_item_page(request):
	response = {}
	response['form'] = item()
	response['type'] = 'tambah'
	response['categories'] = semua_nama_kategori()
	return render(request, 'cu-item.html', response)

def semua_nama_kategori():
	with connection.cursor() as cursor:
		cursor.execute("SELECT NAMA FROM KATEGORI")
		categories_tuple = cursor.fetchall()

		categories = []
		for category in categories_tuple:
			categories.append(category[0])
		return categories

def check_availability(nama):
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM ITEM WHERE nama = %s", [nama])
		ktp_row = cursor.fetchone()
		if ktp_row != None:
			return False
	return True

def delete_item(request, nama):
	if (request.session['role'] == 'Admin'):
		with connection.cursor() as cursor:
			cursor.execute("DELETE FROM ITEM WHERE nama = %s", [nama])
			return redirect("/item")

def update_item_page(request, nama):
	response = {}
	form = item()
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM ITEM WHERE nama = %s", [nama])
		row = cursor.fetchone()
		form.fields['nama'].widget.attrs['placeholder'] = row[0]
		form.fields['deskripsi'].widget.attrs['placeholder'] = row[1]
		form.fields['usia_dari'].widget.attrs['placeholder'] = row[2]
		form.fields['usia_sampai'].widget.attrs['placeholder'] = row[3]
		form.fields['bahan'].widget.attrs['placeholder'] = row[4]

		cursor.execute("SELECT NAMA FROM KATEGORI")
		categories_tuple = cursor.fetchall()

		categories = []
		for category in categories_tuple:
			categories.append(category[0])
		response['categories'] = categories

		response['form'] = form
		response['nama'] = row[0]
		response['type'] = 'update'
		return render(request, 'cu-item.html', response)

def update_item(request, nama_awal):
	form = item(request.POST or None)
	if (request.method == "POST" and form.is_valid()):
			nama = request.POST['nama']
			deskripsi = request.POST['deskripsi']
			usia_dari = request.POST['usia_dari']
			usia_sampai = request.POST['usia_sampai']
			bahan = request.POST['bahan']
			categories = request.POST.getlist('category')
			if (check_availability(nama) or nama == nama_awal):
				with connection.cursor() as cursor:
					cursor.execute("UPDATE ITEM SET nama = %s, deskripsi = %s, usia_dari = %s, usia_sampai = %s, bahan = %s WHERE nama = %s", [nama, deskripsi, usia_dari, usia_sampai, bahan, nama_awal])
					cursor.execute("DELETE FROM KATEGORI_ITEM WHERE nama_item = %s", [nama])
					for category in categories:
						cursor.execute("INSERT INTO KATEGORI_ITEM VALUES(%s, %s)", [nama, category])
				return redirect("/item")
			else:
				form.add_error("deskripsi", "Tidak bisa mendaftarkan item dengan nama tersebut!")
				response = {}
				response['form'] = form
				response['nama'] = nama_awal
				response['type'] = 'update'
				response['categories'] = semua_nama_kategori()
				return render(request, 'cu-item.html', response)
