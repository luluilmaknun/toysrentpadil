from django import forms

class item(forms.Form):
    nama = forms.CharField(label = '', max_length=225, widget = forms.TextInput(attrs={'class': 'form-control Font-Size-20',
        'id': 'nama', 'placeholder': "Nama Item"}))

    deskripsi = forms.CharField(label = '', widget = forms.Textarea(attrs={'class': 'form-control Font-Size-20',
        'id': 'deskripsi', 'placeholder': "Deskripsi", 'rows': '1'}))

    usia_dari = forms.IntegerField(label = '', widget = forms.NumberInput(attrs={'class': 'form-control Font-Size-20',
        'id': 'usia_dari', 'placeholder': "Usia Minimal"}))

    usia_sampai = forms.IntegerField(label = '', widget = forms.NumberInput(attrs={'class': 'form-control Font-Size-20',
        'id': 'usia_sampai', 'placeholder': "Usia Maksimal"}))

    bahan = forms.CharField(label = '', widget = forms.Textarea(attrs={'class': 'form-control Font-Size-20',
        'id': 'bahan', 'placeholder': "Bahan", 'rows': '1'}))

    # deskripsi = forms.CharField(label = '', widget = forms.EmailInput(attrs={'class': 'form-control Font-Size-20',
    #     'id': 'deskripsi', 'placeholder': "Deskripsi"}))
