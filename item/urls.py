from django.conf.urls import url
from django.urls import path
from .views import *
#url for app

app_name = 'pengguna'
urlpatterns = [
    path('', index, name='index'),
    path('tambah', tambah_item_page, name='tambah_item_page'),
    path('tambah-item', tambah_item, name='tambah_item'),
    path('delete/<str:nama>/', delete_item, name='delete_item'),
    path('update/<str:nama>/', update_item_page, name='update_item_page'),
    path('update-item/<str:nama_awal>', update_item, name='update_item'),
]