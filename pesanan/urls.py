from django.urls import path
from pesanan import views

app_name = 'pesanan'
urlpatterns = [
	path('', views.daftar_pesanan, name='daftar_pesanan'),
    path('buat_pesanan', views.buat_pesanan, name='buat_pesanan'),
    path('<int:pk>/update', views.update_pesanan, name='update_pesanan')
]
