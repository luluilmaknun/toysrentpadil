from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect
import datetime

# Create your views here.
def daftar_pesanan(request):
	response = {}
	content = {}
	data_pesanan = []
	sql = "SELECT P.id_pemesanan, R.nama_item, P.harga_sewa, S.nama AS STATUS FROM PEMESANAN P JOIN STATUS S ON S.nama = P.status JOIN BARANG_PESANAN B ON B.id_pemesanan = P.id_pemesanan JOIN BARANG R ON R.id_barang = B.id_barang"

	if request.session["role"] == "Admin":
		with connection.cursor() as cursor:
			cursor.execute(sql)
			data_pesanan = cursor.fetchall()
	elif request.session["role"] == "Anggota":
		with connection.cursor() as cursor:
			sql = sql + " WHERE P.no_ktp_pemesan = %s"
			cursor.execute(sql, [request.session["no_ktp"]])
			data_pesanan = cursor.fetchall()

	for data in data_pesanan:
		if data[0] in response.keys():
			response[data[0]]["barang"].append(data[1])
		else:
			response[data[0]] = {}
			response[data[0]]["id_pemesanan"] = data[0]
			response[data[0]]["barang"] = [data[1]]
			response[data[0]]["harga"] = data[2]
			response[data[0]]["status"] = data[3]

	content["response"] = response
	return render(request, 'daftar_pesanan.html', content)

def buat_pesanan(request):
	response = {}
	current_id_pemesanan = 0

	if (request.method == "POST"):

		with connection.cursor() as cursor:
			sql = "SELECT P.id_pemesanan FROM PEMESANAN P"
			cursor.execute(sql)
			ids = cursor.fetchall()
			for id in ids:
				if int(id[0]) > current_id_pemesanan:
					current_id_pemesanan = int(id[0])

			id_pemesanan = current_id_pemesanan + 1
			no_ktp = request.POST["no_ktp_anggota"]
			id_barang = request.POST.getlist("id_barang")
			kuantitas = len(id_barang)
			lama_sewa = request.POST["lama_sewa"]
			waktu = datetime.datetime.now()
			tanggal = datetime.date.today()
			cursor.execute("INSERT INTO PEMESANAN(id_pemesanan, datetime_pesanan, kuantitas_barang, no_ktp_pemesan, status) VALUES(%s, %s, %s, %s, %s)", [id_pemesanan, waktu, kuantitas, no_ktp, "SEDANG_DIKONFIRMASI"])
			for i in range(len(id_barang)):
				cursor.execute(
					"INSERT INTO BARANG_PESANAN(id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, status) VALUES(%s, %s, %s, %s, %s, %s)",
					[id_pemesanan, i, id_barang[i], tanggal, lama_sewa, "SEDANG_DIKONFIRMASI"])

		return redirect("/pesanan/")

	with connection.cursor() as cursor:
		cursor.execute("SELECT P.no_ktp, P.nama_lengkap FROM ANGGOTA A JOIN PENGGUNA P ON A.no_ktp = P.no_ktp")
		anggota = cursor.fetchall()
		response["anggota"] = anggota

		cursor.execute("SELECT I.nama, B.id_barang FROM BARANG B JOIN ITEM I ON I.nama = B.nama_item")
		barang = cursor.fetchall()
		response["barang"] = barang

	return render(request, 'buat_pesanan.html', response)

def update_pesanan(request, pk):
	data = {}
	response = {}

	if (request.method == "POST"):
		with connection.cursor() as cursor:
			id_barang = request.POST.getlist("id_barang")
			status = request.POST["konfirm"]
			cursor.execute(
				"UPDATE PEMESANAN SET status = %s WHERE id_pemesanan = CAST(%s AS varchar)",
				[status, pk])
			cursor.execute(
				"DELETE FROM PEMESANAN WHERE id_pemesanan = CAST(%s AS varchar)", [pk])
			for i in range(len(id_barang)):
				cursor.execute(
					"INSERT INTO BARANG_PESANAN(id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, status) VALUES(%s, %s, %s, %s, %s, %s)",
					[pk, i, id_barang[i], datetime.date.today(), lama_sewa, status])

		return redirect("/pesanan/")

	with connection.cursor() as cursor:
		cursor.execute("SELECT I.nama, B.id_barang FROM BARANG B JOIN ITEM I ON I.nama = B.nama_item")
		barang = cursor.fetchall()
		data["barang"] = barang
		cursor.execute("SELECT id_barang FROM BARANG_PESANAN WHERE id_pemesanan = CAST(%s AS varchar)", [pk])
		id_barang = cursor.fetchall()
		cursor.execute("SELECT lama_sewa FROM BARANG_PESANAN WHERE id_pemesanan = CAST(%s AS varchar)", [pk])
		lama_sewa = cursor.fetchone()
		cursor.execute("SELECT status FROM PEMESANAN WHERE id_pemesanan = CAST(%s AS varchar)", [pk])
		status = cursor.fetchone()

	data["pk"] = pk
	data["status"] = status[0]
	data["lama_sewa"] = lama_sewa[0]
	data["id_barang"] = id_barang
	print(data)
	return render(request, 'update_pesanan.html', data)