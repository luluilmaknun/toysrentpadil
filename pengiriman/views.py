from django.shortcuts import render
from django.shortcuts import redirect
from django.db import connection
# Create your views here.
def index(request):
	response = {}

	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM PENGIRIMAN")
		response["shipping"] = cursor.fetchall()

	return render(request, 'daftar_pengiriman.html', response)

def pengiriman_tulis(request):
	return render(request, 'form_pengiriman.html', {})

def update(request):
	return render(request, 'update_pengiriman.html', {})