from django.urls import path
from pengiriman import views
from .views import *

app_name = 'pengiriman'
urlpatterns = [
	path('', index, name='index'),
	path('add', pengiriman_tulis, name='tulis'),
	path('update', update, name='update'),
]
