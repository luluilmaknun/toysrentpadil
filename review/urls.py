from django.urls import path
from review import views
from .views import *

app_name = 'pengiriman'
urlpatterns = [
	path('', index, name='index'),
	path('add', tulis, name='tulis'),
	path('update', update, name='update'),
]
