from django.shortcuts import render
from django.shortcuts import redirect
from django.db import connection
# Create your views here.
def index(request):
	response = {}

	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM REVIEW")
		response["reviews"] = cursor.fetchall()

	return render(request, 'daftar_review.html', response)

def tulis(request):
	return render(request, 'tulis_review.html', {})

def update(request):
	return render(request, 'update_review.html', {})